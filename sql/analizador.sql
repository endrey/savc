--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: analisis; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER ROLE seguridad WITH PASSWORD 'seguridad';

CREATE TABLE analisis (
    numero_analisis integer NOT NULL,
    nombre_analisis character varying,
    aplicacion_numero_aplicacion integer,
    informe_numero_informe integer,
    contenido_informe character varying
);


ALTER TABLE public.analisis OWNER TO seguridad;

--
-- Name: aplicacion; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE aplicacion (
    numero_aplicacion integer NOT NULL,
    lenguaje_id_lenguaje integer
);


ALTER TABLE public.aplicacion OWNER TO seguridad;

--
-- Name: aplicacion_vulnerabilidad; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE aplicacion_vulnerabilidad (
    aplicacion_numero_aplicacion integer NOT NULL,
    vulnerabilidad_nombre_vulnerabilidad character varying NOT NULL
);


ALTER TABLE public.aplicacion_vulnerabilidad OWNER TO seguridad;

--
-- Name: informe; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE informe (
    numero_informe integer NOT NULL,
    nombre_informe character varying NOT NULL,
    fecha_emision timestamp without time zone DEFAULT now() NOT NULL,
    informe_generado bytea
);


ALTER TABLE public.informe OWNER TO seguridad;

--
-- Name: lenguaje; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE lenguaje (
    idlenguaje integer NOT NULL,
    nombre_lenguaje character varying NOT NULL
);


ALTER TABLE public.lenguaje OWNER TO seguridad;

--
-- Name: patron; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE patron (
    id_patron integer NOT NULL,
    patron character varying NOT NULL,
    lenguaje_id_lenguaje integer NOT NULL,
    vulnerabilidad_id_vulnerabilidad integer NOT NULL
);


ALTER TABLE public.patron OWNER TO seguridad;

--
-- Name: perfil; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE perfil (
    nombre_perfil character varying(45),
    numero_perfil integer NOT NULL,
    eliminar integer,
    analizar integer,
    registrar integer,
    modificar integer
);


ALTER TABLE public.perfil OWNER TO seguridad;

--
-- Name: usuario; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE usuario (
    cedula integer NOT NULL,
    nombre character varying(20) NOT NULL,
    apellido character varying(20) NOT NULL,
    cargo character varying(15) NOT NULL,
    nombre_user character varying(15) NOT NULL,
    contrasena character varying(50) NOT NULL,
    aplicacion_numero_aplicacion integer,
    nombre_rol character varying(20) NOT NULL,
    analisis_numero_analisis integer,
    numero_rol integer
);


ALTER TABLE public.usuario OWNER TO seguridad;

--
-- Name: usuario_informe; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE usuario_informe (
    usuario_cedula integer NOT NULL,
    informe_numero_informe integer NOT NULL
);


ALTER TABLE public.usuario_informe OWNER TO seguridad;

--
-- Name: usuario_perfil; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE usuario_perfil (
    usuario_cedula integer NOT NULL,
    perfil_numero_perfil integer NOT NULL
);


ALTER TABLE public.usuario_perfil OWNER TO seguridad;

--
-- Name: ver_informe; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE ver_informe (
    cedula_usuario integer NOT NULL,
    informe_ver_numero_informe integer NOT NULL
);


ALTER TABLE public.ver_informe OWNER TO seguridad;

--
-- Name: vulnerabilidad; Type: TABLE; Schema: public; Owner: seguridad; Tablespace: 
--

CREATE TABLE vulnerabilidad (
    nombre_vulnerabilidad character varying NOT NULL,
    id_vulnerabilidad integer NOT NULL,
    lenguaje_id_lenguaje integer
);


ALTER TABLE public.vulnerabilidad OWNER TO seguridad;

--
-- Data for Name: analisis; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY analisis (numero_analisis, nombre_analisis, aplicacion_numero_aplicacion, informe_numero_informe, contenido_informe) FROM stdin;
\.


--
-- Data for Name: aplicacion; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY aplicacion (numero_aplicacion, lenguaje_id_lenguaje) FROM stdin;
\.


--
-- Data for Name: aplicacion_vulnerabilidad; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY aplicacion_vulnerabilidad (aplicacion_numero_aplicacion, vulnerabilidad_nombre_vulnerabilidad) FROM stdin;
\.


--
-- Data for Name: informe; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY informe (numero_informe, nombre_informe, fecha_emision, informe_generado) FROM stdin;
\.


--
-- Data for Name: lenguaje; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY lenguaje (idlenguaje, nombre_lenguaje) FROM stdin;
1	PHP
\.


--
-- Data for Name: patron; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY patron (id_patron, patron, lenguaje_id_lenguaje, vulnerabilidad_id_vulnerabilidad) FROM stdin;
19	((\\$)(\\w+))	1	5
20	.*(\\$\\w+)\\s*=\\s*(escapeshellarg|escapeshellcmd)\n\t\t\t\t\t\t\\(.+\\)	1	5
2	('%s'|\\$\\w+|'"\\.\\$_(GET|POST)\\[('\\w+')\\]\\."'|'\\{\\$_(GET|POST)\\[('\\w+')\\]\\}')	1	1
1	\\"\\s*select(\\s+distinct)?\\s+(\\w+(,\\s*\\w+\\s*)*|\\*)\\s+from\\s+(\\w+\\s+)+(where|having)\\s+(\\w+)\\s*\n\t\t\t\t(=|>|<|>\\s*=|<\\s*=|<\\s*>|!\\s*=|in|is|like|between)\\s*\n\t\t\t\t(\\w+|'\\$\\w+'|\\$\\w+|'%s'|'{\\$\\w+}%'|'\\{\\$_(GET|POST)\\[('\\w+')\\]\\}'|'".\\$_(GET|POST)\\[('\\w+')\\]."')\n\t\t\t\t((\\s+(and|or)\\s+(\\w+|'\\$\\w+'|\\$\\w+|'%s'|'{\\$\\w+}%'|'\\{\\$_(GET|POST)\\[('\\w+')\\]\\}'|'".\\$_(GET|POST)\\[('\\w+')\\]."')\n\t\t\t\t\\s*"?(=|>|<|>\\s*=|<\\s*=|<\\s*>|!\\s*=|in|is|like|between)?\n\t\t\t\t\\s*(\\w+|'\\$\\w+'|\\$\\w+|'%s'|'{\\$\\w+}%'|'\\{\\$_(GET|POST)\\[('\\w+')\\]\\}'|'".\\$_(GET|POST)\\[('\\w+')\\]."')?)*)\n\t\t\t\t\\"?(([\\s,\\n]*((mysql_real_escape_string|floatval|intvalue)?\\((\\$\\w+)\\)\n\t\t\t\t|'{\\$_(GET|POST)\\[('\\w+')\\]\\}'|'".\\$_(GET|POST)\\[('\\w+')\\]."'|\\$\\w+))*);?	1	1
13	[^a-z0-9_](fopen|readfile|file|include|include_once|require|require_once)\\s*\n\t\t\t\t\t\\(?(["']\\s*\\w+)?(["']\\./["']\\.)?\\s*\n\t\t\t\t\t(\\$_GET\\[('\\w+')\\]|\\$\\w+|(\\"pages/(\\$\\w+|\\$_GET\\['(\\w+)'\\])\\")|(\\".+"\\.(\\$\\w+)\\.))\n\t\t\t\t\t(\\.['"](\\..+)['"])?(\\s*['"](\\..+)['"])?(["']\\s*\\))?\\)?	1	4
6	isset\\(	1	2
8	.php|.inc.php	1	3
9	.*((\\$)(\\w+))\\s*=\\s*(str_replace)(.+);$	1	3
10	.*(file_exists)\\s*\\(\\s*((\\$)(\\w+))\\s*\\)	1	3
16	.*((\\$)(\\w+))\\s*=\\s*(str_replace)(.+);$	1	4
15	.*((\\$)(\\w+))\\s*=\\s*(preg_replace)(.+);$	1	4
14	.php|.inc.php	1	4
11	.*(\\$\\w+)\\s*=\\s*(.+)('\\.\\w+')	1	3
12	.*(basename)\\s*\\(\\s*(\\$\\w+)\\s*\\)?(,\\s*\\"\\.\\w+\\"\\))?(\\.PHP_EOL)?	1	3
7	[^a-z0-9_](fopen|readfile|file|include|include_once|require|require_once)\\s*\n\t\t\t\t\t\\(?(["']\\s*\\w+)?(["']\\./["']\\.)?\\s*\n\t\t\t\t\t(\\$_GET\\[('\\w+')\\]|\\$\\w+|(\\"pages/(\\$\\w+|\\$_GET\\['(\\w+)'\\])\\")|(\\".+"\\.(\\$\\w+)\\.))\n\t\t\t\t\t(\\.['"](\\..+)['"])?(\\s*['"](\\..+)['"])?(["']\\s*\\))?\\)?	1	3
22	.*//.*(fopen|readfile|file|include|include_once|require|require_once)\\s*\n\t\t\t\t\t\\(?(["']\\s*\\w+)?(["']\\./["']\\.)?\\s*\n\t\t\t\t\t(\\$_GET\\[('\\w+')\\]|\\$\\w+|(\\"pages/(\\$\\w+|\\$_GET\\['(\\w+)'\\])\\")|(\\".+"\\.(\\$\\w+)\\.))\n\t\t\t\t\t(\\.['"](\\..+)['"])?(\\s*['"](\\..+)['"])?(["']\\s*\\))?\\)?	1	4
23	.*//.*[^a-z0-9_](fopen|readfile|file|include|include_once|require|require_once)\\s*\n\t\t\t\t\t\\(?(["']\\s*\\w+)?(["']\\./["']\\.)?\\s*\n\t\t\t\t\t(\\$_GET\\[('\\w+')\\]|\\$\\w+|(\\"pages/(\\$\\w+|\\$_GET\\['(\\w+)'\\])\\")|(\\".+"\\.(\\$\\w+)\\.))\n\t\t\t\t\t(\\.['"](\\..+)['"])?(\\s*['"](\\..+)['"])?(["']\\s*\\))?\\)?	1	3
5	(isset\\(|(htmlentities|htmlspecialchars|strip_tags)\\(\\n*\\s*(strip_tags\\()?)?\n\t\t\t\t\t((\\$_GET|\\$_POST|\\$_COOKIES|\\$_REQUEST|\\$_SERVER|\n\t\t\t\t\t\\$HTTP_POST_VARS|\\$HTTP_COOKIE_VARS|\\$HTTP_GET_VARS)\n\t\t\t\t\t\\[('\\w+'|\\$\\w+)\\])(\\),\\n*\\s*ENT_QUOTES)?	1	2
3	(mysql_real_escape_string|floatval|intvalue)(\\(\\$\\w+\\)\n\t\t\t|\\(\\$_(GET|POST)\\['\\w+'\\]\\)|\\(\\$_(GET|POST)\\['\\w+'\\]\\))	1	1
24	.*//.*(isset\\(|(htmlentities|htmlspecialchars|strip_tags)\\(\\n*\\s*(strip_tags\\()?)?\n\t\t\t\t\t((\\$_GET|\\$_POST|\\$_COOKIES|\\$_REQUEST|\\$_SERVER|\n\t\t\t\t\t\\$HTTP_POST_VARS|\\$HTTP_COOKIE_VARS|\\$HTTP_GET_VARS)\n\t\t\t\t\t\\[('\\w+'|\\$\\w+)\\])(\\),\\n*\\s*ENT_QUOTES)?	1	2
4	\\s*('\\$\\w+'|\\$\\w+)\\s*=\\s*\n\t\t\t\t(mysql_real_escape_string|floatval|intvalue)(\\(\\$\\w+\\)\n\t\t\t\t|\\(\\$_(GET|POST)\\['\\w+'\\]\\)|\\(\\$_(GET|POST)\\['\\w+'\\]\\)|\\(\\w+\\)|\\('\\w+'\\));?	1	1
18	.*(`\\s*((\\w*(\\\\\\\\)?\\s*\\:?[-./]?)*)\\s*(\\$\\w+\\s*)*`)	1	5
21	.*//.*(((exec|passthru|system|popen|proc_open|shell_exec)\\s*\n\t\t\t\t\t\\(\\s*(['"](([\\s\\w="'.&]*(\\$\\w+)[\\s="'.&]*)|(\\s*\\w+\\s*[-.]?)*(\\s*,\\s*\\$\\w+)*([".']{0,3}\\s*(\\$\\w+)[".*/']{0,3}\\w*)?['"])?\n\t\t\t\t\t(\\s*[,.]\\s*\\$\\w+)*\\s*((\\$\\w+)(\\s*[.,]\\s*['"]\\w+['"])?)?)\\s*\\))|\n\t\t\t\t\t(`\\s*((\\w*(\\\\\\\\)?\\s*\\:?[-./]?)*)\\s*(\\s*\\$\\w+)*`))	1	5
17	.*((exec|passthru|system|popen|proc_open|shell_exec)\\s*\n\t\t\t\t\t\\(\\s*(['"](([\\s\\w="'.&]*(\\$\\w+)[\\s="'.&]*)|(\\s*\\w+\\s*[-.]?)*(\\s*,\\s*\\$\\w+)*([".']{0,3}\\s*(\\$\\w+)[".*/']{0,3}\\w*)?['"])?\n\t\t\t\t\t(\\s*[,.]\\s*\\$\\w+)*\\s*((\\$\\w+)(\\s*[.,]\\s*['"]\\w+['"])?)?)\\s*\\))	1	5
\.


--
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY perfil (nombre_perfil, numero_perfil, eliminar, analizar, registrar, modificar) FROM stdin;
\N	4	1	1	1	1
\N	5	1	1	1	0
\N	6	1	1	1	1
\N	7	0	1	1	1
\N	1	0	1	1	1
\N	8	0	0	0	1
\N	13	0	1	1	1
\N	10	0	1	1	1
\N	3	0	0	0	1
\N	12	1	1	1	1
\N	11	0	1	1	1
\N	2	1	1	1	1
\N	14	1	1	1	1
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY usuario (cedula, nombre, apellido, cargo, nombre_user, contrasena, aplicacion_numero_aplicacion, nombre_rol, analisis_numero_analisis, numero_rol) FROM stdin;
0	Administrador	Actual	cargo1	admin	ba326d8deba07f7653e7b3554645f009	\N	Super Administrador	\N	0
\.


--
-- Data for Name: usuario_informe; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY usuario_informe (usuario_cedula, informe_numero_informe) FROM stdin;
\.


--
-- Data for Name: usuario_perfil; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY usuario_perfil (usuario_cedula, perfil_numero_perfil) FROM stdin;
0	14
\.


--
-- Data for Name: ver_informe; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY ver_informe (cedula_usuario, informe_ver_numero_informe) FROM stdin;
\.


--
-- Data for Name: vulnerabilidad; Type: TABLE DATA; Schema: public; Owner: seguridad
--

COPY vulnerabilidad (nombre_vulnerabilidad, id_vulnerabilidad, lenguaje_id_lenguaje) FROM stdin;
SQL Injection	1	1
XSS	2	1
Remote File Inclusion	3	1
Local File Inclusion	4	1
Remote Command Execution	5	1
\.


--
-- Name: analisis_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY analisis
    ADD CONSTRAINT analisis_pkey PRIMARY KEY (numero_analisis);


--
-- Name: aplicacion_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY aplicacion
    ADD CONSTRAINT aplicacion_pkey PRIMARY KEY (numero_aplicacion);


--
-- Name: aplicacion_vulnerabilidad_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY aplicacion_vulnerabilidad
    ADD CONSTRAINT aplicacion_vulnerabilidad_pkey PRIMARY KEY (aplicacion_numero_aplicacion, vulnerabilidad_nombre_vulnerabilidad);


--
-- Name: informe_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY informe
    ADD CONSTRAINT informe_pkey PRIMARY KEY (numero_informe);


--
-- Name: lenguaje_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY lenguaje
    ADD CONSTRAINT lenguaje_pkey PRIMARY KEY (idlenguaje);


--
-- Name: perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (numero_perfil);


--
-- Name: usuario_informe_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY usuario_informe
    ADD CONSTRAINT usuario_informe_pkey PRIMARY KEY (usuario_cedula, informe_numero_informe);


--
-- Name: usuario_perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY usuario_perfil
    ADD CONSTRAINT usuario_perfil_pkey PRIMARY KEY (usuario_cedula, perfil_numero_perfil);


--
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (cedula);


--
-- Name: usuario_ver_informe_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY ver_informe
    ADD CONSTRAINT usuario_ver_informe_pkey PRIMARY KEY (cedula_usuario, informe_ver_numero_informe);


--
-- Name: vulnerabilidad_pkey; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY patron
    ADD CONSTRAINT vulnerabilidad_pkey PRIMARY KEY (id_patron);


--
-- Name: vulnerabilidad_pkey1; Type: CONSTRAINT; Schema: public; Owner: seguridad; Tablespace: 
--

ALTER TABLE ONLY vulnerabilidad
    ADD CONSTRAINT vulnerabilidad_pkey1 PRIMARY KEY (id_vulnerabilidad);


--
-- Name: public; Type: ACL; Schema: -; Owner: seguridad
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM seguridad;
GRANT ALL ON SCHEMA public TO seguridad;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

