import sys
import psycopg2
import ConfigParser

	
class Conexion:
	
	def __init__(self,orden = None,condicion = None):

		self.orden = orden
		self.condicion = condicion

	def conectar(self):
		
		try:
			config = ConfigParser.ConfigParser()
			config.read("/etc/savc/config.conf")
			base = config.get("db", "conexionbd")
			global conn 
			conn = psycopg2.connect(base)
		except:
			exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
			# Exit the script and print an error telling what happened.
			sys.exit("\nexceptionType -> %s\n excceptionValue->%s" \
			% (exceptionType, exceptionValue))
			
	def cursor(self):
		
		global cur	
		cur = conn.cursor()
		
	def consulta(self,orden,condicion):
		
		if condicion != "":
			cur.execute(orden,condicion)
		else:
			cur.execute(orden)
		lista = cur.fetchall()
		
		return lista
		
	def ejecutar(self,orden,condicion):
	
		if condicion != "":
			cur.execute(orden,condicion)
		else:
			cur.execute(orden)
		
	def commit(self):
				
		conn.commit()
				
	def cerrar(self):
		
		cur.close()
		conn.close()
		




