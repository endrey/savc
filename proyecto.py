import os
import gtk
import pygtk
import psycopg2
import sys
import datos.conexion
import modelo.usuario
import modelo.perfil
import modelo.informe
import modelo.aplicacion
import modelo.analisis



class proyecto():
	
	# Establecer un objeto para su posterior uso
	global TIENE_PERMISO_EN_PERFIL
	global NO_TIENE_PERMISO_EN_PERFIL
	global abrir
	
	TIENE_PERMISO_EN_PERFIL=1
	NO_TIENE_PERMISO_EN_PERFIL=0
	
	def __init__(self):
		
		# Instanciar las clases a usar
		global conex
		conex = datos.conexion.Conexion()		
		global user
		user = modelo.usuario.Usuario()
		global perf
		perf = modelo.perfil.Perfil()
		global infor
		infor = modelo.informe.Informe()
		global aplicacion
		aplicacion = modelo.aplicacion.Aplicacion()
		global analisis
		analisis = modelo.analisis.Analisis()
		global vuln
		vuln = modelo.vulnerabilidad.Vulnerabilidad()
		# Conectar con la base de datos
		conex.conectar()
		conex.cursor()
		
		# Instanciacion de la clase proyecto
		self.glade_file = "gui/proyecto.glade"
		builder = gtk.Builder()
		builder.add_from_file(self.glade_file)
		
		self.bienvenida1 = builder.get_object("bienvenida1")
		self.ingresar1 = builder.get_object("ingresar1")
		self.etiq4 = builder.get_object("etiq4")
		self.usuario1 = builder.get_object("usuario1")
		self.clave2 = builder.get_object("clave2")
		
		self.menu2 = builder.get_object("menu2")
		self.admin21 = builder.get_object("admin21")
		self.analizar22 = builder.get_object("analizar22")
		self.consultar23 = builder.get_object("consultar23")
		self.salir24 = builder.get_object("salir24")
		self.manual25 = builder.get_object("manual25")
		self.imagen22 = builder.get_object("imagen22")
		self.imagen21 = builder.get_object("imagen21")
		self.etiq22 = builder.get_object("etiq22")
		
		self.administrar3 = builder.get_object("administrar3")
		self.registrar31 = builder.get_object("registrar31")
		self.modificar32 = builder.get_object("modificar32")
		self.eliminar33 = builder.get_object("eliminar33")
		self.atras34 = builder.get_object("atras34")
		self.otorgar35 = builder.get_object("otorgar35")
		self.imagen32 = builder.get_object("imagen32")
		self.imagen33 = builder.get_object("imagen33")
		self.imagen34 = builder.get_object("imagen34")
		self.imagen36 = builder.get_object("imagen36")	
		
		self.registrar4 = builder.get_object("registrar4")
		self.atras41 = builder.get_object("atras41")
		self.registrar42 = builder.get_object("registrar42")
		self.etiq411 = builder.get_object("etiq411")
		
		self.nombre41 = builder.get_object("nombre41")
		self.apellido42 = builder.get_object("apellido42")
		self.cedula43 = builder.get_object("cedula43")
		self.nombre_u44 = builder.get_object("nombre_u44")
		self.contrasena45 = builder.get_object("contrasena45")
		self.contrasena_c46 = builder.get_object("contrasena_c46")
		self.cargo41 = builder.get_object("cargo41")
		self.rol42 = builder.get_object("rol42")
		
		self.modificar5 = builder.get_object("modificar5")
		self.atras51 = builder.get_object("atras51")
		self.aceptar52 = builder.get_object("aceptar52")
		self.contrasena53 = builder.get_object("contrasena53")
		self.etiq53 = builder.get_object("etiq53")
		self.tree1 = builder.get_object("tree1")
		self.liststore1 = builder.get_object("liststore1")
		self.tree1.set_model(self.liststore1)
		
		self.eliminar6 = builder.get_object("eliminar6")
		self.atras61 = builder.get_object("atras61")
		self.eliminar_u62 = builder.get_object("eliminar_u62")	
		self.etiq63 = builder.get_object("etiq63")
		self.tree2 = builder.get_object("tree2")
		
		self.confirmborrar7 = builder.get_object("confirmborrar7")
		self.cancelar71 = builder.get_object("cancelar71")
		self.aceptar72 = builder.get_object("aceptar72")
				
		self.cargar8 = builder.get_object("cargar8")
		self.archivo81 = builder.get_object("archivo81")
		self.direc82 = builder.get_object("direc82")
		self.atras83 = builder.get_object("atras83")
		
		self.archselec1 = builder.get_object("archselec1")
		self.dialog1 = builder.get_object("dialog1")
		self.botona1 = builder.get_object("botona1")
		self.botona2 = builder.get_object("botona2")
		self.etiqa1 = builder.get_object("etiqa1")
	
		self.archselec2 = builder.get_object("archselec2")
		self.dialog2 = builder.get_object("dialog2")
		self.botonb1 = builder.get_object("botonb1")
		self.botonb2 = builder.get_object("botonb2")
		self.etiqb1 = builder.get_object("etiqb1")
			
		self.datos_analisis9 = builder.get_object("datos_analisis9")
		self.atras91 = builder.get_object("atras91")
		self.aceptar92 = builder.get_object("aceptar92")
		self.tree_lenguaje = builder.get_object("tree_lenguaje")
		self.tree_vuln = builder.get_object("tree_vuln")
		self.tree_scan = builder.get_object("tree_scan")
		self.analizar1 = builder.get_object("analizar1")
		self.analizar2 = builder.get_object("analizar2")
		self.analizar3 = builder.get_object("analizar3")
			
		self.encurso10 = builder.get_object("encurso10")
		self.cancelar101 = builder.get_object("cancelar101")
		self.iniciar102 = builder.get_object("iniciar102")
		self.aceptar103 = builder.get_object("aceptar103")
		self.bar101 = builder.get_object("bar101")
		self.vbox23 = builder.get_object("vbox23")
		self.nombre_inform101 = builder.get_object("nombre_inform101")
		self.aceptar104 = builder.get_object("aceptar104")
		self.label5 = builder.get_object("label5")
		self.etiq101 = builder.get_object("etiq101")
		self.hbox22 = builder.get_object("hbox22")
			
		self.consultar11 = builder.get_object("consultar11")
		self.abrir111 = builder.get_object("abrir111")
		self.eliminar112 = builder.get_object("eliminar112")
		self.atras113 = builder.get_object("atras113")
		self.tree6 = builder.get_object("tree6")
		self.tree7 = builder.get_object("tree7")
		
		self.borrar_info12=builder.get_object("borrar_info12")
		self.cancelar121=builder.get_object("cancelar121")
		self.aceptar122=builder.get_object("aceptar122")
					
		self.permisos13 = builder.get_object("permisos13")
		self.atras131 = builder.get_object("atras131")
		self.aceptar132 = builder.get_object("aceptar132")
		self.ver133 = builder.get_object("ver133")
		self.registrar131 = builder.get_object("registrar131")
		self.modificar132 = builder.get_object("modificar132")
		self.eliminar133 = builder.get_object("eliminar133")
		self.analizar134 = builder.get_object("analizar134")
		self.tree3 = builder.get_object("tree3")
		
		self.modif_datos14 = builder.get_object("modif_datos14")
		self.etiq1411 = builder.get_object("etiq1411")
		self.etiq1412 = builder.get_object("etiq1412")
		self.atras141 = builder.get_object("atras141")
		self.aceptar142 = builder.get_object("aceptar142")
		self.nombre141 = builder.get_object("nombre141")
		self.apellido142 = builder.get_object("apellido142")
		self.cedula143 = builder.get_object("cedula143")
		self.nombre_u144 = builder.get_object("nombre_u144")
		self.cargo141 = builder.get_object("cargo141")
		self.rol142 = builder.get_object("rol142")
		
		self.permiso_inf15 = builder.get_object("permiso_inf15")
		self.atras151 = builder.get_object("atras151")
		self.aceptar152 = builder.get_object("aceptar152")
		self.tree4 = builder.get_object("tree4")
		self.tree5 = builder.get_object("tree5")
		self.liststore2 = builder.get_object("liststore2")
		self.liststore3 = builder.get_object("liststore3")
		self.liststore4 = builder.get_object("liststore4")
		self.permitir_ver151 = builder.get_object("permitir_ver151")
		
		self.cambio_clave16 = builder.get_object("cambio_clave16")
		self.etiq161 = builder.get_object("etiq161")
		self.atras161 = builder.get_object("atras161")
		self.aceptar162 = builder.get_object("aceptar162")
		self.anterior161 = builder.get_object("anterior161")
		self.nueva162 = builder.get_object("nueva162")
		self.confirmar163 = builder.get_object("confirmar163")
		
		self.bienvenida2 = builder.get_object("bienvenida2")
		self.botond1 = builder.get_object("botond1")
		self.etiqd1 = builder.get_object("etiqd1")
		
		self.analisis_finalizado = builder.get_object("analisis_finalizado")
		self.guardar_informe17 = builder.get_object("guardar_informe17")
		self.abrir_informe17 = builder.get_object("abrir_informe17")
					
		builder.connect_signals(self)
		
		lista1 = gtk.ListStore(str)
		lista1.append(["cargo1"])
		lista1.append(["cargo2"])
		lista1.append(["cargo3"])
		
		self.cargo41.set_model(lista1)
		render = gtk.CellRendererText()
		self.cargo41.pack_start(render,True)
		self.cargo41.add_attribute(render,"text",0)
		
		lista2 = gtk.ListStore(str)
		lista2.append(["Super Administrador"])
		lista2.append(["Administrador"])
		lista2.append(["Consultor"])
		
		self.rol42.set_model(lista2)
		self.rol42.pack_start(render,True)
		self.rol42.add_attribute(render,"text",0)
		
		self.cargo141.set_model(lista1)
		self.cargo141.pack_start(render,True)
		self.cargo141.add_attribute(render,"text",0)
		
		self.rol142.set_model(lista2)
		self.rol142.pack_start(render,True)
		self.rol142.add_attribute(render,"text",0)
				
		
	def on_ingresar1_clicked(self,widget):
		
		# Verificar el ingreso al sistema de un usuario
		nombre_user = self.usuario1.get_text()
		global nombre_user1
		nombre_user1 = nombre_user
		contrasena = self.clave2.get_text()
		
		valor = user.comprobar_datos_entrada(nombre_user,contrasena,False,False,False)
	
		if valor[0]:
			self.etiq4.set_text\
			("Introduzca un nombre de usuario y clave")
		elif valor[1]:
			self.etiq4.set_text("Introduzca un nombre de usuario")
		elif valor[2]:
			self.etiq4.set_text("Introduzca una clave")
		else:
			valor = user.acceder(nombre_user,contrasena,False,False)
			if valor[0]:
				self.etiq4.set_text("Nombre de Usuario Invalido")		
			elif valor[1]:
				self.etiq4.set_text("Clave Invalida")
			else:
				self.etiq4.set_text\
				("Bienvenido Usuario %s" % nombre_user)
								
				self.bienvenida2.show_all()
					
	def on_botond1_clicked(self,widget):
		
		global inicio
		inicio = False
		admin = [('1',)]
		if nombre_user1 == 'admin':
			
			admin1 = conex.consulta("select contrasena from usuario where nombre_user=%s",\
			(nombre_user1,))
			if admin1:
				admin = admin1

		if admin[0][0] == 'ba326d8deba07f7653e7b3554645f009':
			self.bienvenida1.hide()
			self.bienvenida2.hide()	
			self.cambio_clave16.show_all()
			self.atras161.hide()
			inicio = True
			global cedula2
			cedula2 = conex.consulta("select cedula from usuario where nombre_user = %s",\
					(nombre_user1,))
			cedula2 = cedula2[0][0]

		else:
			
			# Obtener la cedula, perfil y rol del usuario logueado para posteriores 
			# comprobaciones y dirigir al usuario al menu principal
			nombre_rol = conex.consulta("select nombre_rol from usuario where nombre_user=%s",\
			(nombre_user1,))
			self.etiq22.set_text("Usted esta logueado como %s" % nombre_rol[0])
			global roles
			roles = nombre_rol[0][0]
			#para la modificacion (cedula1)
			global cedula1
			cedula1 = conex.consulta("select cedula from usuario where nombre_user=%s",\
			(nombre_user1,))
			cedula1 = cedula1[0][0]
			
			self.menu2.show_all()
			global perfil1
			perfil1 = perf.obtener_perfil(cedula1)
			if (int(perfil1[0]) == NO_TIENE_PERMISO_EN_PERFIL and\
			 int(perfil1[1]) == NO_TIENE_PERMISO_EN_PERFIL and \
			 int(perfil1[2]) == NO_TIENE_PERMISO_EN_PERFIL):			
				self.admin21.hide()
				self.imagen21.hide()
			if int(perfil1[3]) == NO_TIENE_PERMISO_EN_PERFIL:
				self.analizar22.hide()
				self.imagen22.hide()
					
			self.bienvenida1.hide()
			self.bienvenida2.hide()		
						
	def on_admin21_clicked(self,widget):
		
		# Dirigir al usuario al menu administrar usuario, comprobando a que 
		# opciones tiene permiso a acceder
		self.administrar3.show_all()
		
		if perfil1[0] == 0:
			self.eliminar33.hide()
			self.imagen34.hide()			
			
		if perfil1[1] == 0:
			self.registrar31.hide()
			self.imagen32.hide()
			
		if perfil1[2] == 0:
			self.modificar32.hide()
			self.imagen33.hide()
			
		if roles != "Super Administrador":
			self.otorgar35.hide()
			self.imagen36.hide()
		
		self.menu2.hide()
			
	def on_analizar22_clicked(self,widget):
		
		# Dirigir al usuario al menu analizar
		
		global es_dir
		es_dir = False
		self.cargar8.show_all()		
		self.menu2.hide()
	
	def on_consultar23_clicked(self,widget):
		
		# Dirigir al usuario a la ventana consultar informe
		self.liststore2.clear()
		self.liststore3.clear()
		
		if roles == "Administrador":
			lista = conex.consulta("select cedula,nombre,apellido from usuario where nombre_user=%s",\
			(nombre_user1,))
			self.liststore2.append\
			([lista[0][0],lista[0][1],lista[0][2]])
		else:
			lista = conex.consulta("select cedula,nombre,apellido from usuario","")	
			for elemento in lista:
					self.liststore2.append([elemento[0],\
					elemento[1],elemento[2]])
			

		self.consultar11.show_all()
		self.menu2.hide()
	
	def on_salir24_clicked(self,widget):
		
		# Salir del sistema
		
		conex.cerrar()
		gtk.main_quit()
		
	def on_registrar31_clicked(self,widget):
		
		# Inicializar los valores que se mostraran en las entradas de texto y 
		# combo box como vacios y mostrar la ventana de registro al usuario
		
		self.nombre41.set_text("")
		self.apellido42.set_text("")
		self.cedula43.set_text("")
		self.nombre_u44.set_text("")
		self.contrasena45.set_text("")
		self.contrasena_c46.set_text("")
		self.cargo41.set_active(-1)
		self.rol42.set_active(-1)
		
		self.registrar4.show_all()
		self.administrar3.hide()
	
	def on_modificar32_clicked(self,widget):
		
		# Comprobar los permisos que tiene el usuario para modificar a otros usuarios:
		# Si es consultor, solo se modifica a si mismo. 
		
		self.liststore1.clear()
		if roles == "Consultor":
			lista = conex.consulta("select nombre,apellido,nombre_user,cedula\
			 from usuario where nombre_user=%s",\
			 (nombre_user1,))
			self.liststore1.append([lista[0][0],lista[0][1],\
			lista[0][2],lista[0][3]])	
		else:	
			lista = conex.consulta("select nombre,apellido,nombre_user,cedula from usuario","")
			for elemento in lista:
				self.liststore1.append([elemento[0],elemento[1],elemento[2],elemento[3]])
				
		self.modificar5.show_all()
		self.administrar3.hide()
		
	def on_eliminar33_clicked(self,widget):
		
		# Dirigir al usuario a la ventana donde podra seleccionar a que usuario desea eliminar
		self.liststore1.clear()
		lista = conex.consulta("select nombre,apellido,nombre_user,cedula from usuario","")
		for elemento in lista:
			self.liststore1.append([elemento[0],elemento[1],elemento[2],elemento[3]])

		self.eliminar6.show_all()
		self.administrar3.hide()
	
	def on_atras34_clicked(self,widget):
		
		# Dirigir al usuario al menu principal
		self.menu2.show_all()

		if (int(perfil1[0]) == NO_TIENE_PERMISO_EN_PERFIL and \
		int(perfil1[1]) == NO_TIENE_PERMISO_EN_PERFIL and \
		int(perfil1[2]) == NO_TIENE_PERMISO_EN_PERFIL):			
			self.admin21.hide()
			self.imagen21.hide()
		if perfil1[3] == 0:
			self.analizar22.hide()
			self.imagen22.hide()
		
		self.administrar3.hide()
		
	def on_otorgar35_clicked(self,widget):
		
		# Dirigir al usuario a la ventana donde puede otorgar permisos a los usuarios.
		self.liststore1.clear()
		lista = conex.consulta("select nombre,apellido,nombre_user,cedula\
		 from usuario","")
		
		for elemento in lista:
			self.liststore1.append([elemento[0],\
			elemento[1],elemento[2],elemento[3]])
			
		self.permisos13.show_all()
		self.administrar3.hide()
		
	def on_atras41_clicked(self,widget):
		
		# Dirigir al usuario al menu Administrar Usuario
		self.administrar3.show_all()
			
		if perfil1[0] == 0:
			self.eliminar33.hide()
			self.imagen34.hide()			
			
		if perfil1[1] == 0:
			self.registrar31.hide()
			self.imagen32.hide()
			
		if perfil1[2] == 0:
			self.modificar32.hide()
			self.imagen33.hide()
			
		if roles != "Super Administrador":
			self.otorgar35.hide()
			self.imagen36.hide()
			
		self.registrar4.hide()
		
	
	def on_registrar42_clicked(self,widget):
		
		# Obtener los datos que el usuario introduce para crear un nuevo usuario
		# y registrarlo en sistema
		nombre = self.nombre41.get_text()
		apellido = self.apellido42.get_text()
		cedula = self.cedula43.get_text()
		nombre_user = self.nombre_u44.get_text()
		contrasena = self.contrasena45.get_text()
		contrasenacomp = self.contrasena_c46.get_text()
		cargo = self.cargo41.get_active_text()
		rol = self.rol42.get_active_text()
		n_rol = self.rol42.get_active()
		
		if nombre_user == "":
			self.etiq411.set_text("Debe introducir todos los datos")
		elif cedula == "":
			self.etiq411.set_text("Debe introducir todos los datos")
		else:
			igual4 = user.validar_datos(cedula)	
			if igual4 == False:
				igual = user.comprobar_datos_registro(nombre_user,cedula,False,False)
				comprobacion = user.seguridad_clave(contrasena,False)
			
				if nombre == "":
						self.etiq411.set_text("Debe introducir todos los datos")
				elif apellido == "":
					self.etiq411.set_text("Debe introducir todos los datos")
				elif igual[0]:
					self.etiq411.set_text("Ya existe un usuario con esa cedula")	
				elif igual[1]:
					self.etiq411.set_text("Ya existe un usuario con ese login")
				elif contrasena == "":
					self.etiq411.set_text("Debe introducir todos los datos")
				elif rol == "":
					self.etiq411.set_text("Debe introducir todos los datos")
				elif self.cargo41.get_active() == -1:
					self.etiq411.set_text("Debe introducir todos los datos")
				elif self.rol42.get_active() == -1:
					self.etiq411.set_text("Debe introducir todos los datos")
				elif comprobacion:
					self.etiq411.set_text("La clave debe constar de minimo 6 valores alfanumericos")
				else:	
					if contrasena == contrasenacomp:
						user.registrar(cedula,nombre,apellido,cargo,nombre_user,contrasena,rol,n_rol)
						self.etiq411.set_text("Usuario Registrado Exitosamente")
						perf.colocar_nueva_id(cedula,rol)																					
					else:
						self.etiq411.set_text("Las claves deben coincidir")		
			else:
				self.etiq411.set_text("La cedula debe ser un valor numerico")	
		
	def on_atras51_clicked(self,widget):
		
		# Dirigir al usuario al menu Administrar Usuario	
		self.administrar3.show_all()
	
		if perfil1[0] == 0:
			self.eliminar33.hide()
			self.imagen34.hide()			
			
		if perfil1[1] == 0:
			self.registrar31.hide()
			self.imagen32.hide()
			
		if perfil1[2] == 0:
			self.modificar32.hide()
			self.imagen33.hide()
			
		if roles != "Super Administrador":
			self.otorgar35.hide()
			self.imagen36.hide()
			
		self.modificar5.hide()
	
		
	def on_aceptar52_clicked(self,widget):
		
		# Colocar los datos del usuario seleccionado para modificar en las entradas
		# de texto.
		(model,iter) = self.tree1.get_selection().get_selected()
		if iter != None:
			global cedula2
			cedula2 = model.get_value(iter,3)
			global cambio_cedula
			cambio_cedula = False
			if cedula2 == cedula1:
				cambio_cedula = True
			global nombre_user2
			nombre_user2 = model.get_value(iter,2)
			lista = conex.consulta("select * from usuario where cedula=%s",\
			(cedula2,))
			self.nombre141.set_text(lista[0][1])
			self.apellido142.set_text(lista[0][2])
			self.cedula143.set_text(str(lista[0][0]))
			self.nombre_u144.set_text(lista[0][4])
			self.rol142.set_active(lista[0][9])		
		
			self.modif_datos14.show_all()
			self.modificar5.hide()
		else:
			self.etiq53.set_text("Debe seleccionar un usuario")
			
	def on_contrasena53_clicked(self,widget):
		
		# Mostrar la ventana para cambio de clave
		(model,iter) = self.tree1.get_selection().get_selected()
		if iter != None:
			global cedula2
			cedula2 = model.get_value(iter,3)	
			self.modificar5.hide()
			self.cambio_clave16.show_all()
		else:
			self.etiq53.set_text("Debe seleccionar un usuario")
				
	def on_atras141_clicked(self,widget):
		
		# Mostrar la ventana de seleccion de usuario para modificacion
		
		self.liststore1.clear()
		if roles == "Consultor":
			lista=conex.consulta("select nombre,apellido,nombre_user,cedula\
			 from usuario where nombre_user=%s",\
			 (nombre_user1,))
			self.liststore1.append([lista[0][0],lista[0][1],\
			lista[0][2],lista[0][3]])	
		else:	
			lista = conex.consulta("select nombre,apellido,nombre_user,cedula from usuario","")
			for elemento in lista:
				self.liststore1.append([elemento[0],elemento[1],elemento[2],elemento[3]])
				
		self.modificar5.show_all()				
		self.modif_datos14.hide()
	
		
	def on_aceptar142_clicked(self,widget):
		
		# Comprobar los datos introducidos para modificar un usuario
		# y llevar a cabo el proceso.
		nombre = self.nombre141.get_text()
		apellido = self.apellido142.get_text()
		cedula = self.cedula143.get_text()
		nombre_user = self.nombre_u144.get_text()
		cargo = self.cargo141.get_active_text()
		rol = self.rol142.get_active_text()
		
		if cedula == "":
			self.etiq1411.set_text("Debe introducir todos los datos")
		elif nombre_user == "":
			self.etiq1411.set_text("Debe introducir todos los datos")
		else:
			igual = user.validar_datos(cedula)
			if igual == False:
				igual = user.comprobar_datos_modificar(nombre_user,cedula,nombre_user2,cedula2,False,False)
				if nombre == "":
					self.etiq1411.set_text("Debe introducir todos los datos")
				elif apellido == "":
					self.etiq1411.set_text("Debe introducir todos los datos")
				elif igual[0]:
					self.etiq1411.set_text("Ya existe un usuario con esa cedula")
				elif igual[1]:
					self.etiq1411.set_text("Ya existe un usuario con ese login")
				elif rol == "":
					self.etiq1411.set_text("Debe introducir todos los datos")
				elif self.cargo141.get_active() == -1:
					self.etiq1411.set_text("Debe introducir todos los datos")
				elif self.rol142.get_active() == -1:
					self.etiq1411.set_text("Debe introducir todos los datos")
				else:
					user.modificar(nombre,apellido,cargo,cedula,nombre_user,rol,cedula2)
					self.etiq1411.set_text("Usuario Modificado Exitosamente")
					if cambio_cedula:
						cedula1 = cedula2
			else:
				self.etiq1411.set_text("La clave debe ser un valor numerico")
		
	def on_atras61_clicked(self,widget):
		
		# Mostrar el menu Administrar Usuario
		
		self.administrar3.show_all()
	
		if perfil1[0] == 0:
			self.eliminar33.hide()
			self.imagen34.hide()			
			
		if perfil1[1] == 0:
			self.registrar31.hide()
			self.imagen32.hide()
			
		if perfil1[2] == 0:
			self.modificar32.hide()
			self.imagen33.hide()
			
		if roles != "Super Administrador":
			self.otorgar35.hide()
			self.imagen36.hide()
			
		self.eliminar6.hide()
		
		
	def on_eliminar_u62_clicked(self,widget):
		
		# Mostrar la ventana de confirmacion de eliminacion de usuario
		global cedula2
		(model,iter) = self.tree2.get_selection().get_selected()
		if iter != None:
			cedula2 = model.get_value(iter,3)
			self.confirmborrar7.show_all()
		else:
			self.etiq63.set_text("Debe seleccionar un usuario")
				
	def on_cancelar71_clicked(self,widget):
		
		# Ocultar la ventana de confirmacion de eliminacion de usuario
		
		self.confirmborrar7.hide()
	
	def on_aceptar72_clicked(self,widget):
		
		# Eliminar un usuario del sistema y dirigirse al menu Administrar Usuario 
		
		user.eliminar(cedula2)
		self.administrar3.show_all()	
		
		if perfil1[0] == 0:
			self.eliminar33.hide()
			self.imagen34.hide()			
			
		if perfil1[1] == 0:
			self.registrar31.hide()
			self.imagen32.hide()
			
		if perfil1[2] == 0:
			self.modificar32.hide()
			self.imagen33.hide()
			
		if roles != "Super Administrador":
			self.otorgar35.hide()
			self.imagen36.hide()
		
		self.confirmborrar7.hide()
		self.eliminar6.hide()
		
			
	def on_archivo81_clicked(self,widget):
		
		#Mostrar el selector de archivos
		self.archselec1.show_all()
		
	def on_direc82_clicked(self,widget):
		
		# Mostrar el selector de directorios
		self.archselec2.show_all()
		
	def on_atras83_clicked(self,widget):
		
		# Dirigir al usuario al menu principal
		
		self.menu2.show_all()
		
		if ( int(perfil1[0]) == NO_TIENE_PERMISO_EN_PERFIL and \
		int(perfil1[1]) == NO_TIENE_PERMISO_EN_PERFIL and \
		int(perfil1[2]) == NO_TIENE_PERMISO_EN_PERFIL):	
			self.admin21.hide()
			self.imagen21.hide()
			
		if int(perfil1[3]) == NO_TIENE_PERMISO_EN_PERFIL:
			self.analizar22.hide()
			self.imagen22.hide()
			
		self.cargar8.hide()
			
	def on_botona1_clicked(self,widget):
		
		# Cancelar la carga del archivo
		self.archselec1.hide()
		
	def on_botona2_clicked(self,widget):
		
		# Permitir al usuario seleccionar el lenguaje y vulnerabilidades a escanear
		global archivo
		archivo = self.archselec1.get_filenames()
		es_arch = aplicacion.verificar_archivo(archivo[0],False)
		
		if es_arch:		
			self.analizar1.clear()
			lista = conex.consulta("select nombre_lenguaje from lenguaje","")
			for elemento in lista:
				self.analizar1.append([elemento[0]])
			self.archselec1.hide()
			self.datos_analisis9.show_all()
			self.cargar8.hide()
		
	def on_botonb1_clicked(self,widget):
		
		# Cancelar la carga del directorio
		
		self.archselec2.hide()
		
	def on_botonb2_clicked(self,widget):
		
		# Permitir al usuario seleccionar el lenguaje y vulnerabilidades a escanear
		
		global archivo
		archivo = self.archselec2.get_filenames()
		global es_dir
		es_dir = aplicacion.verificar_directorio(archivo[0],False)
		if es_dir:
			self.analizar1.clear()
			lista = conex.consulta("select nombre_lenguaje from lenguaje","")
			for elemento in lista:
				self.analizar1.append([elemento[0]])
			archivos = aplicacion.obtener_arch_directorio(archivo[0])
			global scan_dir	
			scan_dir = []
			ext = aplicacion.seleccionar_arch(archivos,archivo[0],scan_dir)
			self.archselec2.hide()
			self.datos_analisis9.show_all()
			self.cargar8.hide()
		
	def on_atras91_clicked(self,widget):
		
		# Mostrar la ventana para la eleccion del tipo de aplicacion a cargar
		self.datos_analisis9.hide()
		self.cargar8.show_all()
		
	def on_aceptar92_clicked(self,widget):
		
		# Mostrar ventana de analisis
		
		global app
		app = aplicacion.id_aplicacion()
		aplicacion.colocar_patron(app,lista_vuln)
		self.bar101.set_fraction(0)	
		self.encurso10.show()
		self.nombre_inform101.set_text("")
		self.nombre_inform101.hide()
		self.aceptar104.hide()
		self.label5.set_text("Introduzca un nombre para el informe y presione aceptar")
		self.label5.hide()
		self.datos_analisis9.hide()
		
		global terminado
		terminado = False
		self.bar101.set_fraction(0.01)
		id_leng = aplicacion.colocar_lenguaje(app,lenguaje)
		if lista_vuln != []:
			scan1 = analisis.seleccionar_scan(lista_vuln)
			global scan_final
			scan_final = [[],[],[],[],[]]
			if es_dir == False:
				scan_final = analisis.scan_archivo(archivo,scan1,id_leng,scan_final)
				self.bar101.set_fraction(1)
			else:
				try:
					index = len(scan_dir)
					index = 1/float(index)
					acumulado = 0
					lista_funciones = []
					for directorio in scan_dir:
						archi = open(directorio,'r')
						data = archi.read()
						lista_funciones = vuln.buscar_funcion(data,lista_funciones,directorio)
						archi.close()
					for i in range(len(scan_dir)):
						scan_final = analisis.scan_directorio\
						(scan_dir[i],scan1,id_leng,scan_final,lista_funciones)
						self.bar101.set_fraction(acumulado+index)
						acumulado = acumulado+index
						while gtk.events_pending():
							gtk.main_iteration()
					self.bar101.set_fraction(1)		
				except:
					self.bar101.set_fraction(1)	
					pass
			
				acumulado = 0

			terminado = True
			self.analisis_finalizado.show_all()
			self.encurso10.hide()
		
	def on_guardar_informe17_clicked(self,widget):
		
		self.encurso10.show()
		self.hbox22.hide()
		self.aceptar104.show()
		self.label5.show()
		self.nombre_inform101.show()
		self.cancelar101.show()
		self.analisis_finalizado.hide()
		
	def on_abrir_informe17_clicked(self,widget):
		
		nombre_i = "Informe de Aplicacion "
		if terminado:
			analisis.datos_analisis(nombre_i,app,scan_final)
			self.menu2.show_all()
			self.encurso10.hide() 
			infor.abrir_informe_generado(nombre_user1,scan_final,cedula1,nombre_i)
		self.analisis_finalizado.hide()
	
	def on_lenguaje91_changed(self,widget):
		
		pass
		
	def on_cancelar101_clicked(self,widget):
		
		# Mostrar la ventana de seleccion de vulnerabilidades
		
		scan_dir = []
		self.encurso10.hide()
		self.datos_analisis9.show_all()		
			
	def on_aceptar104_clicked(self,widget):
		
		# Comprobar el nombre del informe y obtenerlo. Dirigir al usuario al menu principal
		
		nombre_i = self.nombre_inform101.get_text()
		if nombre_i != "":
			igual = infor.verificar_nombre(nombre_i,False)
			if igual == False:
				if terminado:
					analisis.datos_analisis(nombre_i,app,scan_final)
					infor.generar_informe(nombre_user1,scan_final,cedula1,nombre_i)
					self.menu2.show_all()
					self.encurso10.hide()
				if abrir:
					infor.abrir_informe(nombre_i)
			else:
				if terminado:
					self.label5.set_text("Ya existe un informe con ese nombre")

	def on_abrir111_clicked(self,widget):
		
		# Mostrar el informe seleccionado
		
		(model,iter) = self.tree7.get_selection().get_selected()
		if iter != None:
			nombre_inf = model.get_value(iter,0)
		infor.abrir_informe(nombre_inf)
		
	def on_eliminar112_clicked(self,widget):
		
		# Mostrar la ventana de confirmacion de eliminacion de informe
		
		(model,iter) = self.tree7.get_selection().get_selected()
		if iter != None:
			global nombre_informe
			nombre_informe = model.get_value(iter,0)
			self.borrar_info12.show_all()
		
	def on_atras113_clicked(self,widget):
		
		# Mostrar el menu principal del sistema
		
		self.menu2.show_all()
		
		if ( int(perfil1[0]) == NO_TIENE_PERMISO_EN_PERFIL and \
		int(perfil1[1]) == NO_TIENE_PERMISO_EN_PERFIL and \
		int(perfil1[2]) == NO_TIENE_PERMISO_EN_PERFIL):				
			self.admin21.hide()
			self.imagen21.hide()
		
		if perfil1[3] == 0:
			self.analizar22.hide()
			self.imagen22.hide()
			
		self.consultar11.hide()
		
	def on_cancelar121_clicked(self,widget):
		
		# Ocultar la ventana de confirmacion de eliminacion de informe
		
		self.borrar_info12.hide()
	
	def on_aceptar122_clicked(self,widget):
		
		# Eliminar el informe seleccionado
		
		infor.eliminar(nombre_informe)
		self.borrar_info12.hide()
		(model,iter) = self.tree6.get_selection().get_selected()
		if iter != None:
			cedula = model.get_value(iter,0)
			self.liststore3.clear()
			try:
				lista = conex.consulta("SELECT nombre_informe FROM \
				informe,ver_informe,usuario	WHERE numero_informe=informe_ver_numero_informe \
				AND cedula_usuario=cedula AND cedula=%s",(cedula,))
				if lista != []:
					for elemento in lista:
						self.liststore3.append([elemento[0]])
			except:
				exceptionType, exceptionValue,exceptionTraceback=sys.exc_info()	
		else:
			pass	

		
	def on_atras131_clicked(self,widget):
		
		# Mostrar el menu Administrar Usuario
		
		self.permisos13.hide()
		self.administrar3.show_all()
		
	def on_aceptar132_clicked(self,widget):
		
		# Otorgar o quitar permisos al usuario seleccionado
		
		igual = (self.registrar131.get_active(),self.modificar132.get_active(),\
		self.eliminar133.get_active(),self.analizar134.get_active())
		
		(model,iter) = self.tree3.get_selection().get_selected()
		if iter != None:
			cedula = model.get_value(iter,3)
			perf.permisos_usuario(cedula,igual[0],igual[1],igual[2],igual[3])
			if nombre_user1 == model.get_value(iter,2):
				global perfil1
				perfil1 = (int(igual[2]),int(igual[0]),int(igual[1]),int(igual[3]))
				
		self.administrar3.show_all()
		
		if perfil1[0] == 0:
			self.eliminar33.hide()
			self.imagen34.hide()			
					
		if perfil1[1] == 0:
			self.registrar31.hide()
			self.imagen32.hide()
				
		if perfil1[2] == 0:
			self.modificar32.hide()
			self.imagen33.hide()
				
		self.permisos13.hide()
		
		
	def on_ver133_clicked(self,widget):
		
		# Otorgar o quitar permiso para ver informe al usuario seleccionado
		
		(model,iter) = self.tree3.get_selection().get_selected()
		if iter != None:
			global cedula2
			cedula2 = model.get_value(iter,3)					
			self.liststore2.clear()
			
			if roles == "Administrador":
				lista = conex.consulta("select cedula,nombre,apellido from usuario\
				 where nombre_user=%s",(nombre_user1,))
				self.liststore2.append([lista[0][0],lista[0][1],\
				lista[0][2]])
			else:
				lista = conex.consulta("select \
				cedula,nombre,apellido from usuario","")
				
				for elemento in lista:
					self.liststore2.append([elemento[0],\
					elemento[1],elemento[2]])
					
			self.permisos13.hide()
			self.permiso_inf15.show_all()	
		
	def on_atras151_clicked(self,widget):
		
		# Mostrar ventana de seleccion de usuario para modificar permisos
		
		self.permiso_inf15.hide()
		self.permisos13.show_all()
		
	def on_aceptar152_clicked(self,widget):
		
		# Mostrar ventana para otorgar permisos para ver informe
		
		(model,iter) = self.tree5.get_selection().get_selected()
		if iter != None:
			nombre_informe = model.get_value(iter,0)	
			igual1 = self.permitir_ver151.get_active()
			infor.ver_informe(cedula2,nombre_informe,igual1)
		
		self.permiso_inf15.hide()
		self.permisos13.show_all()
		
	def on_atras161_clicked(self,widget):
		
		# Mostrar ventana para modificar usuario
		
		self.modificar5.show_all()
		self.cambio_clave16.hide()
		
	def on_aceptar162_clicked(self,widget):
		
		# Cambiar clave de un usuario
		
		anterior = self.anterior161.get_text()
		nueva = self.nueva162.get_text()
		confirmar = self.confirmar163.get_text()
		
		if (anterior == "" or nueva == "" or confirmar == ""):
			self.etiq161.set_text("Debe llenar todos los campos")
		else:
		
			if nueva == confirmar:
				igual = user.comprobar_clave(anterior,cedula2,False)
				if igual:
					comprobacion = user.seguridad_clave(nueva,False)
					if comprobacion == False:
						user.cambiar_clave(cedula2,nueva)
						self.etiq161.set_text("Clave Cambiada")
						if inicio:
							self.cambio_clave16.hide()
							nombre_rol = conex.consulta("select nombre_rol from usuario where nombre_user=%s",\
							(nombre_user1,))
							self.etiq22.set_text("Usted esta logueado como %s" % nombre_rol[0])
							global roles
							roles = nombre_rol[0][0]
							#para la modificacion (cedula1)
							global cedula1
							cedula1 = conex.consulta("select cedula from usuario where nombre_user=%s",\
							(nombre_user1,))
							cedula1 = cedula1[0][0]
							self.menu2.show_all()
							global perfil1
							perfil1 = perf.obtener_perfil(cedula1)
							if (int(perfil1[0]) == NO_TIENE_PERMISO_EN_PERFIL and\
							 int(perfil1[1]) == NO_TIENE_PERMISO_EN_PERFIL and \
							 int(perfil1[2]) == NO_TIENE_PERMISO_EN_PERFIL):			
								self.admin21.hide()
								self.imagen21.hide()
							if int(perfil1[3]) == NO_TIENE_PERMISO_EN_PERFIL:
								self.analizar22.hide()
								self.imagen22.hide()
					else:
<<<<<<< HEAD
						self.etiq161.set_text("La clave debe constar de minimo 6 valores entre numeros y letras")
=======
						self.etiq161.set_text("La clave debe constar de minimo 6 valores alfanumericos")
>>>>>>> 503a8be16b7a458331f72cb8ac0a7d02519a7b34
				else:
					self.etiq161.set_text("Clave incorrecta")
			else:
				self.etiq161.set_text("Las claves deben coincidir")
			
	def on_combo91_changed(self,widget):
		pass
		
	def on_tree3_cursor_changed(self,widget):
		
		# Mostrar datos del usuario seleccionado
		
		try:	
			(model,iter) = self.tree3.get_selection().get_selected()
			nombre_user = model.get_value(iter,2)
			cedula = model.get_value(iter,3)
			perfil = perf.obtener_perfil(cedula)
			
			if perfil[0] == 0:
				self.eliminar133.set_active(False)
			else:
				self.eliminar133.set_active(True)
				
			if perfil[1] == 0:
				self.registrar131.set_active(False)	
			else:
				self.registrar131.set_active(True)	
				
			if perfil[2] == 0:
				self.modificar132.set_active(False)	
			else:
				self.modificar132.set_active(True)	
				
			if perfil[3] == 0:
				self.analizar134.set_active(False)
			else:
				self.analizar134.set_active(True)
		except:
			pass
		
	def on_tree4_cursor_changed(self,widget):
		
		# Mostrar informes del usuario seleccionado
		
		(model,iter) = self.tree4.get_selection().get_selected()
		if iter != None:
			cedula = model.get_value(iter,0)
			self.liststore4.clear()
			try:
				lista = conex.consulta("SELECT nombre_informe FROM informe,usuario_informe,usuario\
				WHERE informe_numero_informe=numero_informe AND cedula=usuario_cedula AND\
				usuario_cedula=%s",(cedula,))
				if lista != []:
					for elemento in lista:
						self.liststore4.append([elemento[0]])
			except:
				exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
		else:
			pass	
		
	def on_tree5_cursor_changed(self,widget):
		
		# Mostrar permiso del informe seleccionado
		
		(model,iter) = self.tree5.get_selection().get_selected()
		if iter != None:
			nombre_informe = model.get_value(iter,0)
			(model,iter) = self.tree4.get_selection().get_selected()
			cedula = model.get_value(iter,0)
			if iter != None:
				try:
					igual = infor.ver_permiso(cedula2,False,nombre_informe)
					self.permitir_ver151.set_active(igual)	
				except:
					exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
			else:
				pass
		else:
			pass	
		
	def on_tree6_cursor_changed(self,widget):
		
		# Mostrar informes del usuario seleccionado
		
		(model,iter) = self.tree6.get_selection().get_selected()
		if iter != None:
			cedula = model.get_value(iter,0)
			self.liststore3.clear()
			try:
				lista = conex.consulta("SELECT nombre_informe FROM \
				informe,ver_informe,usuario	WHERE numero_informe=informe_ver_numero_informe \
				AND cedula_usuario=cedula AND cedula=%s",(cedula1,))
				lista2 = conex.consulta("SELECT nombre_informe FROM informe,\
				usuario_informe, usuario WHERE numero_informe=informe_numero_informe\
				AND usuario_cedula=cedula AND cedula = %s",(cedula,))
				if lista != [] and lista2 != []:
					for elemento in lista:
						for informe in lista2:
							if elemento == informe:
								self.liststore3.append([elemento[0]])
			except:
				exceptionType, exceptionValue,exceptionTraceback=sys.exc_info()	
		else:
			pass	
				
	def on_tree_lenguaje_cursor_changed(self,widget):
		
		# Mostrar vulnerabilidades del lenguaje seleccionado
		
		(model,iter) = self.tree_lenguaje.get_selection().get_selected()
		if iter != None:
			global lenguaje
			lenguaje = model.get_value(iter,0)
			self.analizar2.clear()
		
			lista = conex.consulta("SELECT nombre_vulnerabilidad FROM \
			vulnerabilidad, lenguaje WHERE lenguaje_id_lenguaje=idlenguaje\
			and nombre_lenguaje=%s",(lenguaje,))
			if lista != []:
				for elemento in lista:
					if elemento != (None,):
						self.analizar2.append([elemento[0]])
			self.analizar3.clear()
			global lista_vuln
			lista_vuln = []

					
	def on_boton93_clicked(self,widget):
		
		# Agregar vulnerabilidades seleccionadas
		
		igual = False		
		(model,iter) = self.tree_vuln.get_selection().get_selected()
		if iter != None:
			vuln = model.get_value(iter,0)
			for elemento in lista_vuln:
				if vuln == elemento:
					igual = True
			if igual == False:
				self.analizar3.append([vuln])
				lista_vuln.append(vuln)
	
	def on_boton94_clicked(self,widget):
		
		# Quitar vulnerabilidad seleccionada
		
		(model,iter) = self.tree_scan.get_selection().get_selected()
		if iter != None:
			vuln = model.get_value(iter,0)
			self.analizar3.remove(iter)
			lista_vuln.remove(vuln)	
			
	def on_manual25_clicked(self,widget):
		
		# Ver manual de usuario del sistema
		
		os.system("yelp /usr/share/gnome/help/savc/es/savc.xml")
	
	def on_bienvenida2_destroy(self,widget):
		
		# Si se cierra la ventana de bienvenida
		
		proyecto().on_botond1_clicked(widget)
			
	def gtk_main_quit(self,*args):
		
		# Cerrar el sistema
		
		conex.cerrar()
		gtk.main_quit()		

if __name__=="__main__":
	app=proyecto()
	gtk.main()
	
	
