import datos.conexion
import os
import ConfigParser

class Aplicacion:
	
	def __init__(self,numero_app=None,nombre_app=None,ruta=None):
		
		self.numero_app = numero_app
		self.nombre_app = nombre_app 
		global conex
		conex = datos.conexion.Conexion()	
				
	def verificar_archivo(self,ruta,igual):
		
# 	Verificar si el elemento a escanear es un archivo 				
		
		if os.path.isfile(ruta):
			igual = True
		
		return igual
			
	def verificar_directorio(self,ruta,igual):		

#	Verificar si el elemento a escanear es un directorio
	
		if os.path.isdir(ruta):
			igual = True
				
		return igual
		
	def obtener_arch_directorio(self,ruta):

# 	Obtener los diferentes archivos y carpetas que posee el directorio

		archivos = os.listdir(ruta)
		return archivos
		
	def id_aplicacion(self):
	
#	Otorgar una identificacion a la aplicacion	
		
		lista = conex.consulta("select max(numero_aplicacion) from aplicacion","")
		if lista[0][0] != None:
			numero_app = lista[0][0]+1
		else:
			numero_app = 1	
		conex.ejecutar("insert into aplicacion (numero_aplicacion) values (%s)",\
		(numero_app,))
		conex.commit()
		
		return numero_app
	
	def seleccionar_arch(self,archivos,ruta,scan_dir):

#	Elegir los archivos que seran analizados (para directorio)
		
		extensiones = []
		ext = []
		
		for elemento in archivos:
			extensiones.append(os.path.splitext(elemento))
			
		ext = Aplicacion().comparar_extension(extensiones,ext)
		scan_dir = Aplicacion().guardar_direccion(ruta,ext,scan_dir)
		
		for elemento in archivos:
			archivo = ruta+'/'+elemento
			if os.path.isdir(archivo):
				arch = Aplicacion().obtener_arch_directorio(archivo)	
				Aplicacion().seleccionar_arch(arch,archivo,scan_dir)
	
				
		for elemento in extensiones:
			archivo = ruta+'/'+elemento[0]+elemento[1]
			
		return scan_dir
		
	def comparar_extension(self,extensiones,ext):
		
# 	Verifico que la extension de cada archivo coincide con las que se 
# 	permite escanear
		
		permitido2=[]
		config = ConfigParser.ConfigParser()
		config.read("/etc/savc/extension.conf")
		permitido = config.items("ext")
		for elemento in permitido:
			permitido2.append(elemento[1])
		permitido = permitido2
		for extension in extensiones:
			igual = False
			
			for elemento in permitido:
				if extension[1] == elemento:
					igual = True
	
			if igual:
				ext.append(extension)
		return ext
		
	def guardar_direccion(self,ruta,ext,scan_dir):
		
#	Guardo la direccion del archivo a analizar
		
		comprobar = []
		archivos = Aplicacion().obtener_arch_directorio(ruta)
		for elemento in archivos:
			comprobar.append(os.path.splitext(elemento))
		
		for elemento in ext:
			for i in range(len(comprobar)):
				if comprobar[i] == elemento:
					scan_dir.append(ruta+'/'+archivos[i])
		
		return scan_dir
	
	def colocar_patron(self,app,lista_vuln):
	
#	Se asignan las vulnerabilidades a la aplicacion a escanear		
	
		conex.ejecutar("insert into aplicacion_vulnerabilidad \
		(aplicacion_numero_aplicacion,vulnerabilidad_nombre_vulnerabilidad) values\
		(%s, %s)", (app,lista_vuln))
		
		conex.commit()
		
	def colocar_lenguaje(self,app,lenguaje):

#	Se asigna el lenguaje en el que se realiza el escaneo
		
		id_leng = conex.consulta("select idlenguaje from lenguaje where nombre_lenguaje=%s",(lenguaje,))
		conex.ejecutar("update aplicacion set lenguaje_id_lenguaje=%s where numero_aplicacion=%s",(id_leng[0][0],app))
		return id_leng
		conex.commit()
		

