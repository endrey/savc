import re


class Vulnerabilidad:
	
	def __init__(self, nombre = None, codigo = None):
				
		self.nombre = nombre
		self.codigo = codigo		
	
	def sql_injection(self,patron_scan,data,result_sqli,lista_funciones,directorio,datos_funciones):
		
		# Busqueda base de vulnerabilidad inyeccion sql
		resultado = []
		lista_buscar = []
		lista_contenido = []
		lista_var = []
		lista_var2 = []
		verificacion = []
		indices = []
		comprobar = []
		
		if datos_funciones != []:
			for dato in datos_funciones:
				if directorio == dato[2]:
					busqueda = re.finditer(""+patron_scan[0][0]+"",dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
					for match in busqueda:
						verificacion.append(match.group())
			
		buscar = re.finditer(""+patron_scan[0][0]+"",data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		for match in buscar:
			lista_buscar.append(match.start())
			lista_contenido.append(match.group())
				
		found = re.findall(""+patron_scan[0][0]+"",data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			
		if found != []:
			for elemento in found:
				comprobar.append(elemento)
			if verificacion != []:	
				for i in range(len(lista_contenido)):
					for elemento in verificacion:
						if elemento == lista_contenido[i]:
							indices.append(i)
				lista_buscar,lista_contenido,comprobar=Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)	
			if lista_buscar != []:
				for i in range(len(lista_buscar)):
					lista_var = Vulnerabilidad().analisis_sqli(patron_scan,lista_buscar[i],lista_var,lista_contenido[i],found[i],data)
					if lista_var != []:
						result_sqli[1] = Vulnerabilidad().encontrar_linea(data,lista_buscar[i],result_sqli[1],lista_contenido[i])
						result_sqli[0].append(lista_contenido[i])	

		return result_sqli											
	
	def funciones_sqli(self,patron_scan,result_fsqli,scan_dir,datos_funciones,data):
		
		lista_buscar = []
		lista_contenido = []
		lista_var = []
		lista_var2 = []
		verificacion = []
		indices = []
		comprobar = []
		for dato in datos_funciones:
			buscar = re.finditer(""+patron_scan[0][0]+"",dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			for match in buscar:
				lista_buscar.append(match.start())
				lista_contenido.append(match.group())
					
			found = re.findall(""+patron_scan[0][0]+"",dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
			if found != []:	
				for i in range(len(lista_buscar)):
					lista_var = Vulnerabilidad().analisis_sqli(patron_scan,lista_buscar[i],lista_var,lista_contenido[i],found[i],dato[1][2])
					if lista_var != []:
						variables_vulnerables,eliminar = Vulnerabilidad().verificar_variables(lista_var,dato[0],dato[1])
					if variables_vulnerables != []:
						variables_vulnerables = Vulnerabilidad().seguridad_documento(variables_vulnerables,dato[0][2],data,patron_scan)
						variables_vulnerables = Vulnerabilidad().encontrar_igualdad(variables_vulnerables,dato[0][2],data,patron_scan)
					if variables_vulnerables != []:
						### el llamado a funcion numero x situado en la ruta x es vulnerable.
						result_fsqli[1] = Vulnerabilidad().encontrar_linea(data,dato[0][2],result_fsqli[1],dato[0][3])
						result_fsqli[0].append(dato[0][3])
		
		return result_fsqli
						
	def analisis_sqli(self,patron_scan,lista_buscar,lista_var,lista_contenido,found,data):
	
		igual2 = False
		lista_var2 = []
		variables = found[12]
		var1 = found[7]
		if variables != []:
			lista_var2 = Vulnerabilidad().encontrar_variables(variables,lista_var2,patron_scan)
			lista_var2 = Vulnerabilidad().encontrar_variables(var1,lista_var2,patron_scan)
		else:
			lista_var2.append(var1)	
		igual = lista_var2.count("'%s'")
		lista_var = []
		if igual != 0:
			lista_var = Vulnerabilidad().encontrar_variables(found[26],lista_var,patron_scan)
			for elemento in lista_var2:
				if elemento != "'%s'":
					lista_var.append(elemento)	
			lista_var2 = lista_var		
		if igual!=0:	
			lista_var = Vulnerabilidad().verificar_seguridad(lista_var,lista_buscar,lista_contenido,patron_scan)
		else:
			lista_var = lista_var2	
		lista_var = Vulnerabilidad().seguridad_documento(lista_var,lista_buscar,data,patron_scan)
		lista_var = Vulnerabilidad().encontrar_igualdad(lista_var,lista_buscar,data,patron_scan)
				
		return lista_var
	
	def encontrar_variables(self,variables_s,lista_var,patron_scan):
		
		# Encontrar variables usadas en la consulta para su posterior verificacion
		
		otro = re.findall(patron_scan[1][0],variables_s,re.IGNORECASE | re.VERBOSE)
		if otro != []:
			for elemento in otro:
				lista_var.append(elemento[0])
		return lista_var
		
	def verificar_seguridad(self,lista_var,match,oracion,patron_scan):
		
		# Buscar dentro de la consulta seguridad de variables
		
		buscar = re.findall(patron_scan[2][0],oracion,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		if buscar != []:
			for x in buscar:	
				for elemento in lista_var:
					if x[1] == "("+elemento+")":
						lista_var.remove(elemento)
		return lista_var
		
	def seguridad_documento(self,lista_var,match_fijo,data,patron_scan):
		
		# Buscar en el documento seguridad de variables
		
		lista_buscar2 = []
		buscar2 = re.finditer(patron_scan[2][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			
		for match in buscar2:
			lista_buscar2.append(match.start())
		
		buscar = re.findall(patron_scan[2][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
						
		if buscar!=[]:
			for i in range(len(buscar)):
				for elemento in lista_var:
					if buscar[i][1] == "("+elemento+")":
						if lista_buscar2[i] <= match_fijo:
							lista_var.remove(elemento)				
		return lista_var
		
	def encontrar_igualdad(self,lista_var,match_fijo,data,patron_scan):
		
		# Encontrar variables aseguradas con otro nombre
		
		lista_buscar2 = []
		buscar2 = re.finditer(patron_scan[3][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		
		for match in buscar2:
			lista_buscar2.append(match.start())
		
		buscar = re.findall(patron_scan[3][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
		if buscar != []:
			for i in range(len(buscar)):
				for item in lista_var:
					if buscar[i][0] == item:
						if lista_buscar2[i] <= match_fijo:
							lista_var.remove(item)	
		return lista_var
		
		
	def xss(self,patron_scan,data,result_xss):
		
		# Busqueda base de vulnerabilidad cross site scripting
				
		lista_buscar = []
		lista_contenido = []
		comprobar = []
		eliminar = []
		indices = []
		buscar = re.finditer(patron_scan[0][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)

		for match in buscar:
			lista_buscar.append(match.start())
			lista_contenido.append(match.group())
					
		found = re.findall(patron_scan[0][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
								
		if found != []:
			for elemento in found:
				comprobar.append(elemento)
				
			eliminar = Vulnerabilidad().identificar_comentarios(data,patron_scan[2][0],eliminar)	
			for i in range(len(lista_buscar)):
				for elemento in eliminar:
					if elemento == lista_buscar[i]:
						indices.append(i)
			lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
			indices = Vulnerabilidad().verificar_sentencia(comprobar,lista_contenido,lista_buscar,patron_scan)
			lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
			observacion_entities = []
			observacion_ENT_QUOTES = []
			indices,observacion_entities,observacion_ENT_QUOTES = \
			Vulnerabilidad().verificar_seguridad_xss\
			(comprobar,lista_contenido,lista_buscar,data,observacion_entities,observacion_ENT_QUOTES)
			lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)	
			for i in range(len(lista_buscar)):
				result_xss[1] = Vulnerabilidad().encontrar_linea(data,lista_buscar[i],result_xss[1],lista_contenido[i])
				result_xss[0].append(lista_contenido[i])
		
		return result_xss
				
	def verificar_sentencia(self,comprobar,lista_contenido,lista_buscar,patron_scan):
		
		# Verificar que la instruccion posee variables 
		
		indice = []
		for i in range(len(lista_contenido)):
			comp = re.findall(patron_scan[1][0],lista_contenido[i],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			if comp != []:
				indice.append(i)
		return indice
		
	def verificar_seguridad_xss(self,comprobar,lista_contenido,lista_buscar,data,observacion_entities,observacion_ENT_QUOTES):
		
		# Comprobar seguridad de variables en la instruccion
		
		indice = []
		for i in range(len(lista_contenido)):
			if comprobar[i][4]=='$_SERVER':
				if comprobar[i][5]!="'PHP_SELF'":	
					indice.append(i)
			if comprobar[i][1] == 'htmlentities':
				observacion_entities.append((lista_buscar[i],lista_contenido[i]))
				indice.append(i)
			if comprobar[i][1] == 'strip_tags':
				indice.append(i)
			if comprobar[i][1] == 'htmlspecialchars':
				if not (re.search("ENT_QUOTES",comprobar[i][6])):
					observacion_ENT_QUOTES.append((lista_buscar[i],lista_contenido[i]))
					indice.append(i)
				if comprobar[i][2] != '':
					indice.append(i)
						
		return indice,observacion_entities,observacion_ENT_QUOTES				
				
	def eliminar_elementos(self,lista_buscar,lista_contenido,comprobar,indices):
		
		# Eliminar los elementos que son seguros de las instrucciones a escanear
		
		lista_contenido2 = []
		lista_buscar2 = []
		comprobar2 = []
		
		for elemento in indices:	
			lista_contenido[elemento] = ""
			comprobar[elemento] = ""
			lista_buscar[elemento] = ""
		for i in range(len(lista_contenido)):
			if lista_contenido[i] != "":
				lista_contenido2.append(lista_contenido[i])
			if lista_buscar[i] != "":
				lista_buscar2.append(lista_buscar[i])
			if comprobar[i] != "":
				comprobar2.append(comprobar[i])	
		return lista_buscar2,lista_contenido2,comprobar2	
		
	def rfi(self,patron_scan,data,result_rfi,lista_funciones,directorio,datos_funciones):
		
		# Busqueda base de vulnerabilidad remote file inclusion
		
		lista_buscar = []
		lista_contenido = []
		comprobar = []
		primera_medida = []
		indices = []
		indices2 = []
		eliminar = []
		lista_var = []
		lfi = []
		verificacion = []
		
		if datos_funciones != []:
			for dato in datos_funciones:
				if directorio == dato[2]:
					busqueda = re.finditer(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
					for match in busqueda:
						verificacion.append(match.group())	
		mo = re.findall(patron_scan[0][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
							
		if mo != []:
			for elemento in mo:
				if elemento[4] != '':
					lista_var.append((elemento[4],))
				elif elemento[6] != '':
					lista_var.append((elemento[6],))
				else:			
					lista_var.append((elemento[3],))
				comprobar.append(elemento)
					
			buscar = re.finditer(patron_scan[0][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
					
			for match in buscar:
				lista_buscar.append(match.start())
				lista_contenido.append(match.group())
			if verificacion != []:	
				for i in range(len(lista_contenido)):
					for elemento in verificacion:
						if elemento == lista_contenido[i]:
							indices.append(i)
				lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
				lista_var = Vulnerabilidad().borrar_variables(lista_var,indices)
			eliminar = Vulnerabilidad().identificar_comentarios(data,patron_scan[6][0],eliminar)	
			indices = []
			for i in range(len(lista_buscar)):
				for elemento in eliminar:
					if elemento == lista_buscar[i]:
						indices.append(i)
			if indices != []:
				lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
				lista_var = Vulnerabilidad().borrar_variables(lista_var,indices)
			
			if lista_buscar!=[]:
				lista_buscar,lista_contenido,comprobar,lista_var,lfi,primera_medida = Vulnerabilidad().analisis_rfi\
				(lista_buscar,lista_contenido,comprobar,indices,patron_scan,data,primera_medida,lista_var,lfi)
			if lista_buscar != []:
				for i in range(len(lista_buscar)):
					result_rfi[1] = Vulnerabilidad().encontrar_linea(data,lista_buscar[i],result_rfi[1],lista_contenido[i])
					result_rfi[0].append(lista_contenido[i])
		return result_rfi
			
	def funciones_rfi(self,patron_scan,result_frfi,scan_dir,datos_funciones,data):
	
		lista_buscar = []
		lista_contenido = []
		lista_var = []
		lista_var2 = []
		verificacion = []
		indices = []
		comprobar = []
		primera_medida = []
		lfi2 = []
		variables_vulnerables = []

		for dato in datos_funciones:
			buscar = re.finditer(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			for match in buscar:
				lista_buscar.append(match.start())
				lista_contenido.append(match.group())
					
			found = re.findall(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			
			if found != []:
				for elemento in found:
					if elemento[4] != '':
						lista_var.append((elemento[4],))
					elif elemento[6] != '':
						lista_var.append((elemento[6],))
					else:			
						lista_var.append((elemento[3],))
					comprobar.append(elemento)
				
			if found != []:	
				lista_buscar,lista_contenido,comprobar,lista_var,lfi2,primera_medida = Vulnerabilidad().analisis_rfi\
				(lista_buscar,lista_contenido,comprobar,indices,patron_scan,dato[1][2],primera_medida,lista_var,lfi2)
				if lista_buscar != []:
					for i in range(len(lista_var)):
						variables_vulnerablesx,eliminar = Vulnerabilidad().verificar_variables(lista_var[i],dato[0],dato[1])
						if variables_vulnerablesx != []:
							variables_vulnerables.append((variables_vulnerablesx[0],))
				if variables_vulnerables != []:
					indices = Vulnerabilidad().comprobar_seguridad\
					(lista_buscar,indices,patron_scan,data,variables_vulnerables)
					indices = Vulnerabilidad().comprobar_seguridad2\
					(lista_buscar,indices,patron_scan,data,variables_vulnerables)
					if indices != []:
						for elemento in indices:
							for i in range(len(lista_contenido)):
								if elemento == lista_buscar[i]:
									indices2.append(lista_buscar.index(elemento))
						lista_buscar,lista_contenido,comprobar=Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices2)
						variables_vulnerables = borrar_variables(variables_vulnerables,indices2)
				if variables_vulnerables != []:
					### el llamado a funcion numero x situado en la ruta x es vulnerable.
					result_frfi[1] = Vulnerabilidad().encontrar_linea(data,dato[0][2],result_frfi[1],dato[0][3])
					result_frfi[0].append(dato[0][3])
		
		return result_frfi

	def analisis_rfi(self,lista_buscar,lista_contenido,comprobar,indices,patron_scan,data,primera_medida,lista_var,lfi):
	
		indices2=[]
		
		for i in range(len(lista_buscar)):
			primera_medida,lfi,indices = Vulnerabilidad().verificar_extension\
			(comprobar[i],lista_buscar[i],lista_contenido[i],primera_medida,lfi,patron_scan,indices)	
		indices = Vulnerabilidad().comprobar_seguridad\
		(lista_buscar,indices,patron_scan,data,lista_var)
		indices = Vulnerabilidad().comprobar_seguridad2\
		(lista_buscar,indices,patron_scan,data,lista_var)
		if indices != []:
			for elemento in indices:
				for i in range(len(lista_contenido)):
					if elemento == lista_buscar[i]:
						indices2.append(lista_buscar.index(elemento))
			lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices2)
			lista_var = Vulnerabilidad().borrar_variables(lista_var,indices2)
			
		return lista_buscar,lista_contenido,comprobar,lista_var,lfi,primera_medida
		
	def verificar_extension(self,comprobar,numero,oracion,primera_medida,lfi,patron_scan,indices):
		
		# Comprobar que se coloque la extension al archivo que se desea incluir
		buscar = re.findall(patron_scan[1][0],oracion,re.IGNORECASE |re.VERBOSE | re.MULTILINE)				
		if buscar != []:		
			primera_medida.append((numero,oracion))
		if comprobar[2] != '':
			lfi.append((numero,oracion))	
			indices.append(numero)
		return primera_medida,lfi,indices
		
	def comprobar_seguridad(self,lista_buscar,indices,patron_scan,data,lista_var):
		
		# Comprobar la seguridad de las instrucciones analizadas
		
		numero = []
		numero2 = []
		linea = []
		linea2 = []
		
		seguro = re.findall(patron_scan[2][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		
		buscar = re.finditer(patron_scan[2][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
		for match in buscar:
			numero.append(match.start())
			linea.append(match.group())
			
		buscar2 = re.finditer(patron_scan[3][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
		for match in buscar2:
			numero2.append(match.start())
			linea2.append(match.group())
					
		seguro2 = re.findall(patron_scan[3][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
		for i in range(len(lista_buscar)):														
			if seguro != []:
				for num in range(len(seguro)):
					if seguro[num][0] == lista_var[i][0] and numero[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])
					elif "'"+seguro[num][2]+"'" == lista_var[i][0] and numero[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])
						
			if seguro2 != []:
				for num in range(len(seguro2)):
					if seguro2[num][1] == lista_var[i][0] and numero2[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])
					elif "'"+seguro2[num][3]+"'" == lista_var[i][0] and numero2[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])
					elif seguro2[num][3] == lista_var[i][0] and numero2[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])		
					
		return indices

		
	def comprobar_seguridad2(self,lista_buscar,indices,patron_scan,data,lista_var):
		
		# Comprobar la seguridad de las instrucciones ecaneadas por medio de otra comprobacion
		
		numero = []
		linea = []
		numero2 = []
		linea2 = []
		
		seguro = re.findall(patron_scan[4][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
	
		buscar = re.finditer(patron_scan[4][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			
		for match in buscar:
			numero.append(match.start())
			linea.append(match.group())
		
		seguro2 = re.findall(patron_scan[5][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
						
		buscar = re.finditer(patron_scan[5][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
		for match in buscar:
			numero2.append(match.start())
			linea2.append(match.group())
		
		for i in range(len(lista_buscar)):			
				
			if seguro != []:
				for num in range(len(seguro)):
					if seguro[num][0] == lista_var[i][0] and numero[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])

			if seguro2 != []:
				for num in range(len(seguro2)):
					if seguro2[num][1] == lista_var[i][0] and numero2[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])
	
		return indices
		
	def lfi(self,patron_scan,data,result_lfi,lista_funciones,directorio,datos_funciones):
		
		# Busqueda base de vulnerabilidad inyeccion local file inclusion
		
		lista_buscar = []
		lista_contenido = []
		comprobar = []
		primera_medida = []
		indices = []
		lfi = []
		indices2 = []
		eliminar = []
		lfi_indices = []
		lfi_ind = []
		lista_var = []
		verificacion = []
		
		if datos_funciones != []:
			for dato in datos_funciones:
				if directorio == dato[2]:
					busqueda = re.finditer(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
					for match in busqueda:
						verificacion.append(match.group())		
		
		mo = re.findall(patron_scan[0][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		
		if mo!= []:
			for elemento in mo:
				if elemento[4] != '':
					lista_var.append((elemento[4],))
				if elemento[6] != '':
					lista_var.append((elemento[6],))
				else:			
					lista_var.append((elemento[3],))
				comprobar.append(elemento)
				
			buscar = re.finditer(patron_scan[0][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)			
			for match in buscar:
				lista_buscar.append(match.start())
				lista_contenido.append(match.group())
			if verificacion != []:	
				for i in range(len(lista_contenido)):
					for elemento in verificacion:
						if elemento == lista_contenido[i]:
							indices.append(i)
				lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
				lista_var = Vulnerabilidad().borrar_variables(lista_var,indices)
			
			eliminar = Vulnerabilidad().identificar_comentarios(data,patron_scan[4][0],eliminar)	
			indices = []
			for i in range(len(lista_buscar)):
				for elemento in eliminar:
					if elemento == lista_buscar[i]:
						indices.append(i)
			if indices != []:
				lista_buscar,lista_contenido,comprobar=Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
				lista_var = Vulnerabilidad().borrar_variables(lista_var,indices)
			
			if lista_buscar != []:
				lista_buscar,lista_contenido,comprobar,lista_var,lfi,primera_medida=Vulnerabilidad().analisis_lfi\
				(lista_buscar,lista_contenido,comprobar,indices,patron_scan,data,primera_medida,lista_var,lfi)
			if lista_buscar != []:
				for i in range(len(lista_buscar)):
					result_lfi[1] = Vulnerabilidad().encontrar_linea(data,lista_buscar[i],result_lfi[1],lista_contenido[i])		
					result_lfi[0].append(lista_contenido[i])	
			
		return result_lfi
	
	def funciones_lfi(self,patron_scan,result_flfi,scan_dir,datos_funciones,data):
	
		lista_buscar = []
		lista_contenido = []
		lista_var = []
		lista_var2 = []
		verificacion = []
		indices = []
		comprobar = []
		primera_medida = []
		variables_vulnerables = []
		lfi2 = []

		for dato in datos_funciones:
			buscar = re.finditer(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			for match in buscar:
				lista_buscar.append(match.start())
				lista_contenido.append(match.group())
					
			found = re.findall(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			
			if found!= []:
				for elemento in found:
					if elemento[4] != '':
						lista_var.append((elemento[4],))
					elif elemento[6] != '':
						lista_var.append((elemento[6],))
					else:			
						lista_var.append((elemento[3],))
					comprobar.append(elemento)
				
			if found!= []:	
				lista_buscar,lista_contenido,comprobar,lista_var,lfi2,primera_medida = Vulnerabilidad().analisis_lfi\
				(lista_buscar,lista_contenido,comprobar,indices,patron_scan,dato[1][2],primera_medida,lista_var,lfi2)
				if lista_buscar != []:
					for i in range(len(lista_var)):
						variables_vulnerablesx,eliminar = Vulnerabilidad().verificar_variables(lista_var[i],dato[0],dato[1])
						if variables_vulnerablesx != []:
							variables_vulnerables.append((variables_vulnerablesx[0],))
				if variables_vulnerables != []:
					indices = Vulnerabilidad().comprobar_seguridad_lfi\
					(lista_buscar,indices,patron_scan,data,variables_vulnerables)
					if indices != []:
						for elemento in indices:
							for i in range(len(lista_contenido)):
								if elemento == lista_buscar[i]:
									indices2.append(lista_buscar.index(elemento))
						lista_buscar,lista_contenido,comprobar=Vulnerabilidad().eliminar_elementos\
						(lista_buscar,lista_contenido,comprobar,indices2)
						variables_vulnerables = borrar_variables(variables_vulnerables,indices2)
				if variables_vulnerables != []:
					### el llamado a funcion numero x situado en la ruta x es vulnerable.
					result_flfi[1] = Vulnerabilidad().encontrar_linea(data,dato[0][2],result_flfi[1],dato[0][3])
					result_flfi[0].append(dato[0][3])
		
		return result_flfi
		
	def analisis_lfi(self,lista_buscar,lista_contenido,comprobar,indices,patron_scan,data,primera_medida,lista_var,lfi):
						
		indices2 = []
		
		for i in range(len(lista_buscar)):
			primera_medida,lfi = Vulnerabilidad().verificar_extension_lfi\
			(comprobar[i],lista_buscar[i],lista_contenido[i],primera_medida,lfi,patron_scan)		
		indices = Vulnerabilidad().comprobar_seguridad_lfi\
		(lista_buscar,indices,patron_scan,data,lista_var)
		if indices != []:
			for elemento in indices:
				for i in range(len(lista_contenido)):
					if elemento == lista_buscar[i]:
						indices2.append(lista_buscar.index(elemento))
			lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices2)
			lista_var = Vulnerabilidad().borrar_variables(lista_var,indices2)
			
		return lista_buscar,lista_contenido,comprobar,lista_var,lfi,primera_medida
		
	def verificar_extension_lfi(self,comprobar,numero,oracion,primera_medida,lfi,patron_scan):
		
		# Comprobar que se coloque la extension al archivo que se desea incluir
		
		buscar = re.findall(patron_scan[1][0],oracion,re.IGNORECASE |re.VERBOSE | re.MULTILINE)				
		if buscar != []:		
			primera_medida.append((numero,oracion))
		if comprobar[2] != '':
			lfi.append((numero,oracion))	
			
		return primera_medida,lfi
		
	def comprobar_seguridad_lfi(self,lista_buscar,indices,patron_scan,data,lista_var):
									
		# Comprobar que existan las instrucciones de seguridad en las instrucciones analizadas
		
		numero = []
		numero2 = []
		linea = []
		linea2 = []
		
		seguro=re.findall(patron_scan[2][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		
		buscar = re.finditer(patron_scan[2][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
		for match in buscar:
			numero.append(match.start())
			linea.append(match.group())
			
		seguro2 = re.findall(patron_scan[3][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		
		buscar2 = re.finditer(patron_scan[3][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
		for match in buscar2:
			numero2.append(match.start())
			linea2.append(match.group())			

		for i in range(len(lista_buscar)):
			if seguro != []:
				for num in range(len(seguro)):
					if seguro[num][0] == lista_var[i][0] and numero[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])
					elif "'"+seguro[num][2]+"'" == lista_var[i][0] and numero[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])	
			
			if seguro2 != []:
				for num in range(len(seguro2)):
					if seguro2[num][0] == lista_var[i][0] and numero2[num] <= lista_buscar[i]:
						indices.append(lista_buscar[i])
							
		return indices
		
	def rce(self,patron_scan,data,result_rce,lista_funciones,directorio,datos_funciones):
		
		# Busqueda base de vulnerabilidad remote command execution
				
		lista_buscar = []
		lista_contenido = []
		comprobar = []
		eliminar = []
		indices = []
		verificacion = []
		if datos_funciones != []:
			for dato in datos_funciones:
				if directorio == dato[2]:
					busqueda = re.finditer(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
					for match in busqueda:
						verificacion.append(match.group())
						
		buscar = re.finditer(patron_scan[0][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)		
		for match in buscar:
			lista_buscar.append(match.start())
			lista_contenido.append(match.group())
		mo = re.findall(patron_scan[0][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)					
		if mo!= []:
			for elemento in mo:
				comprobar.append(elemento)		
		if comprobar != []:	
		
			if verificacion != []:	
				for i in range(len(lista_contenido)):
					for elemento in verificacion:
						if elemento == lista_contenido[i]:
							indices.append(i)
				lista_buscar,lista_contenido,comprobar = Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
			if lista_contenido != []:
				eliminar = Vulnerabilidad().identificar_comentarios(data,patron_scan[4][0],eliminar)	
				indices = []
				for i in range(len(lista_buscar)):
					for elemento in eliminar:
						if elemento == lista_buscar[i]:
							indices.append(i)
				if indices != []:
					lista_buscar,lista_contenido,comprobar=Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
			if lista_buscar != []:	
				for i in range(len(lista_buscar)):
					lista_var = Vulnerabilidad().analisis_rce(comprobar[i],patron_scan,data,lista_buscar[i])
					if lista_var != []:
						result_rce[1] = Vulnerabilidad().encontrar_linea(data,lista_buscar[i],result_rce[1],lista_contenido[i])
						result_rce[0].append(lista_contenido[i])
		return result_rce
	
	def funciones_rce(self,patron_scan,result_frce,scan_dir,datos_funciones,data):
		
		lista_buscar = []
		lista_contenido = []
		lista_var = []
		lista_var2 = []
		verificacion = []
		indices = []
		comprobar = []
		eliminar = []
		
		for dato in datos_funciones:
			buscar = re.finditer(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			for match in buscar:
				lista_buscar.append(match.start())
				lista_contenido.append(match.group())
					
			found = re.findall(patron_scan[0][0],dato[1][2],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			if found != []:
				for elemento in found:
					comprobar.append(elemento)
			if comprobar != []:	
				eliminar=Vulnerabilidad().identificar_comentarios(dato[1][2],patron_scan[4][0],eliminar)	
				for i in range(len(lista_buscar)):
					for elemento in eliminar:
						if elemento == lista_buscar[i]:
							indices.append(i)
				if indices != []:
					lista_buscar,lista_contenido,comprobar=Vulnerabilidad().eliminar_elementos(lista_buscar,lista_contenido,comprobar,indices)
				
				for i in range(len(lista_buscar)):
					lista_var = Vulnerabilidad().analisis_rce(comprobar[i],patron_scan,dato[1][2],lista_buscar[i])
					if lista_var != []:
						variables_vulnerables,eliminar=Vulnerabilidad().verificar_variables(lista_var,dato[0],dato[1])
					if variables_vulnerables != []:
						variables_vulnerables=Vulnerabilidad().verificar_seguridad_rce(variables_vulnerables,dato[0][2],data,patron_scan)
					if variables_vulnerables != []:
						### el llamado a funcion numero x situado en la ruta x es vulnerable.
						result_frce[1]=Vulnerabilidad().encontrar_linea(data,dato[0][2],result_frce[1],dato[0][3])
						result_frce[0].append(dato[0][3])
		
		return result_frce
		
	def analisis_rce(self,comprobar,patron_scan,data,lista_buscar):
		
		lista_var = []
		lista_var = Vulnerabilidad().encontrar_variables_rce(comprobar[0],lista_var,patron_scan)
		lista_var = Vulnerabilidad().verificar_seguridad_rce(lista_var,lista_buscar,data,patron_scan)		
		return lista_var
						
	def encontrar_variables_rce(self,instruccion,lista_var,patron_scan):
		
		# Encontrar variables presentes en la instruccion analizada
		
		otro = re.findall(patron_scan[2][0],instruccion,re.IGNORECASE | re.VERBOSE)
		if otro != []:
			for elemento in otro:
				lista_var.append(elemento[0])
		return lista_var
		
	def verificar_seguridad_rce(self,lista_var,match_fijo,data,patron_scan):
		
		# Verificar que las variables encontradas sean seguras
		
		lista_buscar = []
		lista_contenido = []
		
		buscar2 = re.finditer(patron_scan[3][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		
		for match in buscar2:
			lista_buscar.append(match.start())
			lista_contenido.append(match.group())
		
		buscar=re.findall(patron_scan[3][0],data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			
		if buscar != []:				
			for i in range(len(buscar)):
				for variable in lista_var:
					if buscar[i][0] == variable:
						if lista_buscar[i] <= match_fijo:
							lista_var.remove(variable)
		return lista_var
		
	def identificar_comentarios(self,data,patron_scan,eliminar):
		
		# Descartar las instrucciones que se encuentran comentadas para evitar falsos positivos
			
		buscar = re.finditer(patron_scan,data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
					
		for match in buscar:
			eliminar.append(match.start())
		
		return eliminar
		
	def encontrar_linea(self,data,lista,numeros,oracion):
	
		# Identificar los numeros de linea donde se encuentran las instrucciones no seguras
	
		una_lista = []
		lista_comprobacion = []
		buscar = re.finditer("""\n$""",data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		for match in buscar:
			una_lista.append(match.start())	

		buscar2 = re.finditer("""^\n""",oracion,re.IGNORECASE |re.VERBOSE| re.MULTILINE)	
		for match in buscar2:
			lista_comprobacion.append(match.start())
		num = 1
		if lista_comprobacion != [0]:
			num = 0
		for i in range(len(una_lista)):
			if num == 0:
				if lista == una_lista[i]:
					numeros.append(una_lista.index(una_lista[i])+2)
					break
			else:
				if i != len(una_lista)-1:
					num = una_lista[i+1]-una_lista[i]
					while num != 0:
						if lista == una_lista[i]+num:
							numeros.append(una_lista.index(una_lista[i])+2)
							break
						num = num-1
					num = 1
				else: 
					num = 50
					while num != 0:
						if lista == una_lista[i]+num:
							numeros.append(una_lista.index(una_lista[i])+2)
							break
						num = num-1
					num = 1
		return numeros
		
	def buscar_funcion(self,data,funcion_variables,directorio):
		
		# Identificar las funciones presentes en el documento y obtener sus variables
		#~ function\s*(\w+)\(((\w+,?\s*|\$\w+,?\s*)+)\)(\s|\n)*\{(.*?)(?=\})
		fx = []
		variable_funcion=[]
		buscar0 = re.finditer("""function\s*(\w+)\((((&)?\w+,?\s*|(&)?\$\w+\s*(=\s*('|")?(\#|@|\.|\*|%|\w|~|&|$|-|:|\(|\)|\[|\]|"|'|,|\s|/|\\\)*('|")?\s*)?,?\s*)+)\)(\s|\n)*;?(\{(.*?)(?=\}))?"""
				,data,re.VERBOSE |re.DOTALL)
		for i in buscar0:
			fx.append((i.group(),i.start()))
		if fx != []:
			for elemento in fx:
				search1=re.findall("""^function.+;"""
						,elemento[0],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				if search1 == []:				
					buscar2 = re.findall("""function\s*(\w+)\((((&)?\w+,?\s*|(&)?\$\w+\s*(=\s*('|")?(\#|@|\.|\*|%|\w|~|&|$|-|:|\(|\)|\[|\]|"|'|,|\s|/|\\\)*('|")?\s*)?,?\s*)+)\)"""
							,elemento[0],re.IGNORECASE |re.VERBOSE | re.MULTILINE)		
					if buscar2 != []:
						variable_funcion.append((buscar2[0][0],buscar2[0][1],elemento[0],elemento[1]))
			if variable_funcion != []:
				for sentencia in variable_funcion:
					variables = []
					buscar_v = re.findall("""(\$\w+|(?<!(?=.|=..|=...))\w+)""",sentencia[1],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
					if buscar_v != []:
						for variable in buscar_v:
							variables.append(variable)
						funcion_variables.append((sentencia[0],variables,sentencia[2],sentencia[3],directorio))	
					
		return funcion_variables
		
	def verificar_llamado_funcion(self,data,lista_funciones):
		
		# Chequear si se realiza un llamado a una funcion y obtener sus variables
		
		num = []
		buscar_v = []
		llamado_funcion_variables = []
		variable_funcion = []
		variable_funcion_pasada = []				
		buscarx=re.finditer("""(=\s*|^\s*|^\t|\s*\w+\s*->\s*)((^\w)?\w+)\((?<!\n)(('\w+',?\s*|\w+,?\s*|\$\w+,?\s*)+)\)"""
					,data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)			
		for match in buscarx:
			num.append((match.start(),match.group()))	
		buscar3 = re.findall("""(=\s*|^\s*|^\t|\s*\w+\s*->\s*)((^\w)?\w+)\((?<!\n)(('\w+',?\s*|\w+,?\s*|\$\w+,?\s*)+)\)"""
				,data,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
				
		if buscar3 != []:
			for i in range(len(buscar3)):
				variable_funcion_pasada.append((buscar3[i][1],buscar3[i][3],num[i][0],num[i][1]))
		
		for funcion in variable_funcion_pasada:
			igual = False
			for nombre_funcion in lista_funciones:
				if funcion[0] == nombre_funcion[0]:
					igual = True
			if igual:
				variable_funcion.append(funcion)	
					
		for sentencia in variable_funcion:
			variables = []
			buscar_v = re.findall("""(\$\w+|\w+)""",sentencia[1],re.IGNORECASE |re.VERBOSE | re.MULTILINE)
			
			if buscar_v != []:
				for variable in buscar_v:
					variables.append(variable)
				llamado_funcion_variables.append((sentencia[0],variables,sentencia[2],sentencia[3]))		
		
		return llamado_funcion_variables	
	
	def verificar_variables(self,variables_analizadas,funcion,nombre_funcion):
		
		# Verificar que en el llamado a la funcion se utilicen variables como parametros
		variable2=[]
		variable_vulnerable = []
		eliminar = []
		
		for elemento in nombre_funcion[1]:
			for i in range(len(variables_analizadas)):
				if elemento == variables_analizadas[i]:
					eliminar.append(i)
					indice = nombre_funcion[1].index(variables_analizadas[i])
					variable2.append(funcion[1][indice])
					
		if variable2 != []:	
			for elemento in variable2:
				identificar_variable = re.findall("""\$""",elemento,re.IGNORECASE |re.VERBOSE | re.MULTILINE)	
				if identificar_variable != []:
					variable_vulnerable.append(elemento)			
	
		return variable_vulnerable,eliminar
		
	def comprobar_coincidencia(self,lista_funciones,var_fun):
		
		for nombre_funcion in lista_funciones:
			if var_fun[0] == nombre_funcion[0]:
				directorio_funcion = nombre_funcion[4]
				return nombre_funcion,directorio_funcion
		
	def borrar_variables(self,lista3,eliminar):
		
		lista_var2=[]
		
		for elemento in eliminar:	
			lista3[elemento] = ""
		for i in range(len(lista3)):
			if lista3[i] != "":
				lista_var2.append(lista3[i])
				
		return lista_var2	
						
					

						
