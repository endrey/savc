import vulnerabilidad
import datos.conexion
import os
import re

class Analisis:
	
	def __init__(self,numero_analisis = None):
		
		self.numero_analisis = numero_analisis
		global conex
		conex = datos.conexion.Conexion()
		global vuln
		vuln = vulnerabilidad.Vulnerabilidad()	
		
	def seleccionar_scan(self,lista_vuln):

#	Seleccion de las vulnerabilidades a escanear
		
		scan1=[]
		for elemento in lista_vuln:
			scan = conex.consulta("select id_vulnerabilidad from vulnerabilidad where\
			nombre_vulnerabilidad=%s",(elemento,))
			scan1.append(scan[0][0])	
		return scan1
			
	def scan_archivo(self,archivo,scan1,id_leng,scan_final):
					
#	Escaneo del archivo seleccionado.
		
		datos_funciones = []
		variable_funcion = []
		
		arch=open(archivo[0],'r')
		data=arch.read()
		
		lista_funciones = vuln.buscar_funcion(data,variable_funcion,archivo[0])
		if lista_funciones!=[]:
			variable_funcion = vuln.verificar_llamado_funcion(data,lista_funciones)
		if variable_funcion != []:
			for var_fun in variable_funcion:
				nombre_funcion = []
				nombre_funcion,directorio_funcion = vuln.comprobar_coincidencia(lista_funciones,var_fun)
				if nombre_funcion != []:
					datos_funciones.append((var_fun,nombre_funcion,directorio_funcion))

		for vulner in scan1:
			patron_scan = conex.consulta("SELECT patron FROM patron WHERE vulnerabilidad_id_vulnerabilidad=%s\
				AND lenguaje_id_lenguaje=%s ORDER BY id_patron ASC",(vulner,id_leng[0][0]))
			if vulner == 1:
				result_sqli = [[],[]]
				result_fsqli = [[],[]]
				result_sqli = vuln.sql_injection\
				(patron_scan,data,result_sqli,lista_funciones,archivo[0],datos_funciones)
				if datos_funciones != []:
					result_fsqli = vuln.funciones_sqli\
					(patron_scan,result_fsqli,archivo[0],datos_funciones,data)
				if result_sqli[0] != []:
					scan_final[0].append((archivo[0],result_sqli[1],result_sqli[0]))
				if result_fsqli[0] != []:
					scan_final[0].append((archivo[0],result_fsqli[1],result_fsqli[0]))
				del result_sqli, result_fsqli
			if vulner == 2:
				result_xss = [[],[]]
				result_xss = vuln.xss\
				(patron_scan,data,result_xss)
				if result_xss[0] != []:
					scan_final[1].append((archivo[0],result_xss[1],result_xss[0]))
				del result_xss
			if vulner == 3:
				result_rfi = [[],[]]
				result_frfi = [[],[]]
				result_rfi = vuln.rfi\
				(patron_scan,data,result_rfi,lista_funciones,archivo[0],datos_funciones)
				if datos_funciones != []:
					result_frfi = vuln.funciones_rfi\
					(patron_scan,result_frfi,archivo[0],datos_funciones,data)
				if result_rfi[0] != []:
					scan_final[2].append((archivo[0],result_rfi[0],result_rfi[1]))
				if result_rfi[0] != []:
					scan_final[2].append((archivo[0],result_frfi[0],result_frfi[1]))
				del result_rfi, result_frfi
			if vulner == 4:
				result_lfi = [[],[]]
				result_flfi = [[],[]]
				result_lfi = vuln.lfi\
				(patron_scan,data,result_lfi,lista_funciones,archivo[0],datos_funciones)
				if datos_funciones != []:
					result_flfi = vuln.funciones_lfi\
					(patron_scan,result_flfi,archivo[0],datos_funciones,data)
				if result_lfi[0] != []:
					scan_final[3].append((archivo[0],result_lfi[0],result_lfi[1]))
				if result_flfi[0] != []:
					scan_final[3].append((archivo[0],result_flfi[0],result_flfi[1]))
				del result_lfi, result_flfi
			if vulner == 5:
				result_rce = [[],[]]
				result_frce = [[],[]]
				result_rce=vuln.rce\
				(patron_scan,data,result_rce,lista_funciones,archivo[0],datos_funciones)
				if datos_funciones != []:
					result_frce = vuln.funciones_rce\
					(patron_scan,result_frce,archivo[0],datos_funciones,data)
				if result_rce[0] != []:
					scan_final[4].append((archivo[0],result_rce[0],result_rce[1]))
				if result_frce[0] != []:
					scan_final[4].append((archivo[0],result_frce[0],result_frce[1]))
				del result_rce, result_frce
		arch.close()
		return scan_final
	
	def scan_directorio(self,scan_dir,scan1,id_leng,scan_final,lista_funciones):
							
#	Escaneo del directorio seleccionado		

		datos_funciones = []
		variable_funcion = []
				
		arch = open(scan_dir,'r')
		data = arch.read()
		if lista_funciones != []:
			variable_funcion = vuln.verificar_llamado_funcion(data,lista_funciones)

		if variable_funcion != []:
			for var_fun in variable_funcion:
				nombre_funcion = []
				nombre_funcion,directorio_funcion = vuln.comprobar_coincidencia(lista_funciones,var_fun)
				if nombre_funcion != []:
					datos_funciones.append((var_fun,nombre_funcion,directorio_funcion))
		for vulner in scan1:
			patron_scan = conex.consulta("SELECT patron FROM patron WHERE vulnerabilidad_id_vulnerabilidad=%s\
										AND lenguaje_id_lenguaje=%s ORDER BY id_patron ASC",(vulner,id_leng[0][0]))
			if vulner == 1:
				result_sqli = [[],[]]
				result_fsqli = [[],[]]
				result_sqli = vuln.sql_injection\
				(patron_scan,data,result_sqli,lista_funciones,scan_dir,datos_funciones)
				if datos_funciones != []:
					result_fsqli = vuln.funciones_sqli\
					(patron_scan,result_fsqli,scan_dir,datos_funciones,data)
				if result_sqli[0] != []:
					scan_final[0].append((scan_dir,result_sqli[1],result_sqli[0]))
				if result_fsqli[0] != []:
					scan_final[0].append((scan_dir,result_fsqli[1],result_fsqli[0]))	
				del result_sqli,result_fsqli		
			if vulner == 2:
				result_xss = [[],[]]
				result_xss = vuln.xss\
				(patron_scan,data,result_xss)
				if result_xss[0] != []:
					scan_final[1].append((scan_dir,result_xss[1],result_xss[0]))
				del result_xss
			if vulner == 3:
				result_rfi = [[],[]]
				result_frfi = [[],[]]
				result_rfi = vuln.rfi\
				(patron_scan,data,result_rfi,lista_funciones,scan_dir,datos_funciones)
				if datos_funciones != []:
					result_frfi = vuln.funciones_rfi\
					(patron_scan,result_rfi,scan_dir,datos_funciones,data)
				if result_rfi[0] != []:
					scan_final[2].append((scan_dir,result_rfi[1],result_rfi[0]))
				if result_frfi[0] != []:
					scan_final[2].append((scan_dir,result_frfi[1],result_frfi[0]))
				del result_rfi,result_frfi
			if vulner == 4:
				result_lfi = [[],[]]
				result_flfi = [[],[]]
				result_lfi = vuln.lfi\
				(patron_scan,data,result_lfi,lista_funciones,scan_dir,datos_funciones)
				if datos_funciones != []:
					result_flfi = vuln.funciones_lfi\
					(patron_scan,result_flfi,scan_dir,datos_funciones,data)
				if result_lfi[0] != []:
					scan_final[3].append((scan_dir,result_lfi[1],result_lfi[0]))
				if result_flfi[0] != []:
					scan_final[3].append((scan_dir,result_flfi[1],result_flfi[0]))
				del result_lfi,result_flfi
			if vulner == 5:
				result_rce = [[],[]]
				result_frce = [[],[]]
				result_rce = vuln.rce\
				(patron_scan,data,result_rce,lista_funciones,scan_dir,datos_funciones)
				if datos_funciones != []:
					result_frce = vuln.funciones_rce\
					(patron_scan,result_frce,scan_dir,datos_funciones,data)
				if result_rce[0] != []:
					scan_final[4].append((scan_dir,result_rce[1],result_rce[0]))
				if result_frce[0] != []:
					scan_final[4].append((scan_dir,result_frce[1],result_frce[0]))
				del result_rce,result_frce
			arch.close()
			
		return scan_final

	def datos_analisis(self,nombre_i,app,scan_final):
		
		# Insercion de datos a la tabla analisis		
		lista = conex.consulta("select max(numero_informe) from informe","")
		if lista[0][0] != None:
			n_informe = lista[0][0]+1
		else:
			n_informe = 1
		conex.ejecutar("insert into analisis \
		(numero_analisis,nombre_analisis,aplicacion_numero_aplicacion,informe_numero_informe,contenido_informe)\
		values (%s,%s,%s,%s,%s)",(app,nombre_i,app,n_informe,str(scan_final)))
		conex.commit()
