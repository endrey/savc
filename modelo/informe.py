import datos.conexion
import modelo.pdf
import psycopg2
import os
from pyfpdf import FPDF

class Informe:
	
	def __init__(self):
		
		global conex
		conex = datos.conexion.Conexion("","")
		global pdf
		pdf = modelo.pdf.PDF()
		
	def eliminar(self,nombre_informe):
		
#	Borrar todos los datos correspondientes a un informe seleccionado
		
		numero = conex.consulta("SELECT numero_informe FROM informe WHERE nombre_informe=%s",(nombre_informe,))	
		conex.ejecutar("delete from informe where nombre_informe=%s",\
		(nombre_informe,))
		conex.ejecutar("DELETE FROM ver_informe WHERE informe_ver_numero_informe=%s",(numero[0],))
		conex.ejecutar("DELETE FROM usuario_informe WHERE informe_numero_informe=%s",(numero[0],))
		conex.commit()
		
	def ver_permiso(self,cedula2,igual,nombre_informe):

#	Verificar si el ususario posee permiso para ver el informe seleccionado.
		
		lista = conex.consulta("SELECT nombre_informe FROM \
				informe,ver_informe,usuario	WHERE informe_ver_numero_informe = numero_informe \
				AND cedula_usuario = cedula AND cedula = %s",(cedula2,))
		for elemento in lista:
			if elemento[0] == nombre_informe:
				igual=True
		return igual
		
	def ver_informe(self,cedula2,nombre_informe,igual1): 
	
#	Colocar permisos al usuario para ver un informe determinado.
	
		numero_informe = conex.consulta("SELECT numero_informe FROM informe WHERE\
		nombre_informe = %s",(nombre_informe,))
		num = conex.consulta("SELECT cedula_usuario FROM ver_informe WHERE\
		informe_ver_numero_informe = %s and cedula_usuario = %s",(numero_informe[0],cedula2))
		if num != []:
			if igual1:
				pass
			else:
				conex.ejecutar("DELETE FROM ver_informe WHERE cedula_usuario = %s\
				and informe_ver_numero_informe = %s",(cedula2,numero_informe[0]))
		else:
			if igual1:
				conex.ejecutar("INSERT INTO ver_informe (cedula_usuario,informe_ver_numero_informe)\
				values (%s,%s)",(cedula2,numero_informe[0]))
			else:
				conex.ejecutar("DELETE FROM ver_informe WHERE cedula_usuario = %s\
				and informe_ver_numero_informe = %s",(cedula2,numero_informe[0]))
		conex.commit()
		
		
	
	def verificar_nombre(self,nombre_i,igual):
		
#	Verificar que no exista el nombre que se desea asignar al informe.
		
		lista=conex.consulta("select numero_informe from informe where nombre_informe=%s",(nombre_i,))
		if lista != []:
			igual = True
		return igual 
		
	def generar_informe(self,autor,scan_final,cedula1,nombre_i):
		
		# Generar un informe para el usuario con base en los resultados
		# obtenidos durante el desarrollo del analisis
		
		ruta = pdf.imprimir_pdf(autor,scan_final,nombre_i)
		lista = conex.consulta("select max(numero_informe) from informe","")
		if lista[0][0] != None:
			n_informe = lista[0][0]+1
		else:
			n_informe = 1
		info=open(ruta,'r').read()
		conex.ejecutar("insert into informe\
		(numero_informe,nombre_informe,informe_generado)\
		values (%s,%s,%s)",
		(n_informe,nombre_i,psycopg2.Binary(info)))
		conex.ejecutar("insert into usuario_informe\
		(usuario_cedula,informe_numero_informe) values (%s,%s)",
		(cedula1,n_informe))
		conex.ejecutar("insert into ver_informe\
		(cedula_usuario,informe_ver_numero_informe) values (%s,%s)",
		(cedula1,n_informe))
		os.remove(ruta)
		conex.commit()	
		return n_informe	
	
	def abrir_informe(self,nombre_inf):
	
		# Abrir un informe para la revision del usuario
		
		informe=conex.consulta("select informe_generado from informe where nombre_informe = %s",(nombre_inf,))
		open('/tmp/informe.pdf', 'wb').write(str(informe[0][0]))
		os.system('/usr/bin/gnome-open /tmp/informe.pdf')
		
	def abrir_informe_generado(self,autor,scan_final,cedula1,nombre_i):
		
		ruta = pdf.imprimir_pdf(autor,scan_final,nombre_i)
		os.system('/usr/bin/gnome-open /tmp/informe.pdf')
	
		
		
