import datos.conexion
import sys
import hashlib
import re

class Usuario:
	
	def __init__(self,cedula=None,nombre_user=None,contrasena=None,\
	nombre=None,apellido=None,cargo=None,rol=None,contrasenacomp=None):
		
		global conex
		conex = datos.conexion.Conexion()	
		self.cedula = cedula
		self.nombre_user = nombre_user
		self.contrasena = contrasena
		self.rol = rol
		self.nombre = nombre
		self.cargo = cargo
		self.apellido = apellido
		self.contrasenacomp = contrasenacomp
		
		
	def comprobar_datos_entrada(self,nombre_user,contrasena,igual,igual1,igual2):
		
		# Verificar que no se hayan introducido campos vacios.
		if nombre_user == "":
			if contrasena == "":
				igual = True
			else:
				igual1 = True
		elif contrasena == "":
			igual2 = True

		return igual,igual1,igual2
		
	def acceder(self,nombre_user,contrasena,igual1,igual2):	
	
		# Verificar que los datos del usuario son correctos para
		# tener acceso al sistema.
		SALT = "28EN"
		hash = hashlib.md5(SALT + contrasena).hexdigest()

		lista = conex.consulta("select cedula from usuario\
		where nombre_user=%s",(nombre_user,))
		if lista == []:
			igual1 = True
		else:
			self.cedula = lista[0][0]
			lista = conex.consulta("select contrasena from usuario\
			where cedula=%s",(self.cedula,))
			
			if lista[0][0] == hash:
				igual2 = False
			else:
				igual2 = True	
		return igual1,igual2

	def comprobar_datos_registro(self,nombre_user,cedula,igual1,igual2):
		
		# Comprobar que los datos que se introdujeron para el registro 
		# del usuario son correctos
		lista = conex.consulta("select cedula from usuario\
		 where nombre_user = %s",(nombre_user,))
		if lista != []:
			igual2 = True
		else:
			pass
			
		lista = conex.consulta("select nombre_user from usuario\
		 where cedula = %s",(cedula,))
		if lista != []:
			igual1 = True
		else:
			pass	
		return igual1,igual2
	
	def validar_datos(self,cedula):
		
		try:
			int(cedula)
			igual4 = False
		except:
			igual4 = True
			pass
		return igual4
		
	def registrar(self,cedula,nombre,apellido,cargo,nombre_user,contrasena,rol,n_rol):	
		
		# Registro de un nuevo usuario en el sistema
		
		SALT = "28EN"
		hash = hashlib.md5(SALT + contrasena).hexdigest()
					
		try:	
			conex.ejecutar("insert into usuario\
			(cedula,nombre,apellido,cargo,nombre_user,contrasena,nombre_rol,numero_rol)\
			values (%s,%s,%s,%s,%s,%s,%s,%s)",
			(cedula,nombre,apellido,cargo,\
			nombre_user,hash,rol,n_rol))
			conex.commit()
#			self.etiq411.set_text("Usuario Registrado Exitosamente")

		except:
			exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
			# Exit the script and print an error telling what happened.
			sys.exit("\nexceptionType -> %s\n excceptionValue->%s"\
			 % (exceptionType, exceptionValue))
		
	def comprobar_datos_modificar(self,nombre_user,cedula,nombre_user2,cedula2,igual1,igual2):		
		
		# Comprobar que los datos que el usuario desea modificar
		# son correctos.
		
		lista = conex.consulta("select cedula from usuario \
		where nombre_user =%s",(nombre_user,))
		if lista != []:
			if lista[0][0] != int(cedula2):
				igual2 = True
			else:
				pass
		else:
			pass

		lista = conex.consulta("select nombre_user from usuario \
		where cedula =%s",(cedula,))
		if lista != []:
			if lista[0][0] != nombre_user2:
				igual1 = True
			else:
				pass
		else:
			pass
		
		return igual1,igual2
		
	def comprobar_clave(self,anterior,cedula,igual):
		
		# Comprobar que la clave del usuario es correcta para su
		# modificacion
		SALT = "28EN"
		hash = hashlib.md5(SALT + anterior).hexdigest()
	
		contrasena = conex.consulta("select contrasena from usuario where cedula=%s",(cedula,))
		if hash == contrasena[0][0]:
			igual = True
		return igual
			
	def modificar(self,nombre,apellido,cargo,cedula,nombre_user,rol,cedula2):
		
		# Modificar los datos existentes del usuario seleccionado
		try:
			conex.ejecutar("""update usuario_perfil set
			usuario_cedula = %s where usuario_cedula = %s""",
			(cedula,cedula2))
			conex.ejecutar("""update usuario set 
			nombre = %s,
			apellido = %s,
			cargo = %s,
			cedula = %s,
			nombre_user = %s, 
			nombre_rol = %s where cedula = %s""",
			(nombre,apellido,cargo,cedula,\
			nombre_user,rol,cedula2))
			
			conex.ejecutar("UPDATE usuario_informe set \
			usuario_cedula= %s where usuario_cedula=%s",(cedula,cedula2,))
			
			conex.ejecutar ("UPDATE ver_informe set \
			cedula_usuario=%s where cedula_usuario=%s",(cedula,cedula2,))
			
			conex.commit()
				
		except:
			exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
			# Exit the script and print an error telling what happened.
			sys.exit("\nexceptionType -> %s\n excceptionValue->%s"\
			 % (exceptionType, exceptionValue))
			#except Exception, e:
			#	print e.pgerror
			
	def cambiar_clave(self,cedula,nueva):
		
		# Realizar el cambio de clave del usuario en sistema
		SALT = "28EN"
		hash = hashlib.md5(SALT + nueva).hexdigest()
		
		try:
			conex.ejecutar("update usuario set contrasena=%s where cedula=%s",(hash,cedula))
			conex.commit()	
		except:
			exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
			sys.exit("\nexceptionType -> %s\n excceptionValue->%s"\
			 % (exceptionType, exceptionValue))
			 
	def seguridad_clave(self,clave,comprobacion):
		
		buscar = re.findall("""[0-9]""",clave,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
		if len(buscar) == 0:
			comprobacion = True

		buscar2 = re.findall("""[a-z]""",clave,re.IGNORECASE |re.VERBOSE | re.MULTILINE)
<<<<<<< HEAD
		if len(buscar2) == 0:
=======
		if len(buscar2) == 0:)
>>>>>>> 503a8be16b7a458331f72cb8ac0a7d02519a7b34
			comprobacion = True
		
		if len(clave) <= 5:
			comprobacion = True
		
		return comprobacion
		
	def eliminar(self,cedula2):
		
		# Eliminar todos los datos de un usuario en el sistema
		try:	
			conex.ejecutar("delete from usuario where cedula = %s",\
			(cedula2,))

			conex.ejecutar("DELETE FROM perfil AS p USING usuario_perfil AS up,usuario AS u\
			WHERE p.numero_perfil = up.perfil_numero_perfil AND up.usuario_cedula = u.cedula \
			AND cedula = %s",(cedula2,))
			
			conex.ejecutar("DELETE FROM usuario_informe \
			WHERE usuario_cedula= %s",(cedula2,))
			
			conex.ejecutar("DELETE FROM ver_informe \
			WHERE cedula_usuario=%s",(cedula2,))
			
			conex.ejecutar("DELETE FROM usuario_perfil USING usuario AS u \
			WHERE usuario_cedula = u.cedula AND cedula = %s",(cedula2,))			
			
			conex.commit()
		except:		
			exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
			# Exit the script and print an error telling what happened.
			sys.exit("\nexceptionType -> %s\n excceptionValue->%s"\
			 % (exceptionType, exceptionValue))
			#except Exception, e:
			#	print e.pgerror
			
	

		
