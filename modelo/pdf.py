#!/usr/bin/python
# -*- coding: latin-1 -*-

from pyfpdf import FPDF

class PDF(FPDF):	
	
	def header(self):
		
		# Encabezado del documento
		self.image('/usr/share/savc/gui/imagenes/ministerio.png',15,8,180)
	
		  
	def footer(self):
		
		# Pie de pagina  
		# Posicion a 1,5 cm del final
		self.set_y(-15)
		self.image('/usr/share/savc/gui/imagenes/bajo.png',10,265,180)
		self.set_font('Arial','I',8)
		# numero de pagina
		self.cell(0,17,'Pagina '+str(self.page_no())+'/{nb}',0,0,'C')
		      
	def titulo_pdf(self,nombre_i):
		
		# Titulo del informe
		self.add_page()
		self.set_font('Arial','B',15)
		w=self.get_string_width(nombre_i)+6
		self.set_x((210-w)/2)
		self.cell(w,0,nombre_i,'C')
		self.ln(12)
		      
	def titulo_vuln(self,num,nombre):
		
		# Titulo de la vulnerabilidad
		self.set_font('Arial','',12)
		self.set_fill_color(200,220,255)
		w=self.get_string_width("Vulnerabilidad %d : %s"%(num,nombre))+5
		self.cell(w,6,"Vulnerabilidad %d : %s"%(num,nombre),0,1,'L',1)
		self.ln(2)
		
	def ruta_vuln(self,ruta):
		
		# Imprimir la ruta de la vulnerabilidad
		self.set_font('Arial','',12)
		self.set_fill_color(180,180,180)
		self.cell(0,10,'Resultados para el elemento situado en la ruta:',0,1)
		self.ln(2)
		w=self.get_string_width(ruta)+5
		self.cell(w,8,ruta,0,1,'L',1)
		self.ln(2)
					
	def contenido_vuln(self,contenido,numeros):
		
		# Imprimir el contenido de la vulnerabilidad
		self.set_font('Times','',12)
		for i in range(len(contenido)):
			self.multi_cell(0,5,"%s : %s" % (str(contenido[i]),str(numeros[i])))
			self.ln()

	def imprimir_vuln(self,ruta,numeros,contenido,cambio_ruta):
		
		# Imprimir todo el documento
		if cambio_ruta != ruta:
			self.ruta_vuln(ruta)
		self.contenido_vuln(numeros,contenido)
		self.set_font('','I')
		self.cell(0,5,'(fin de vulnerabilidad)')
		self.ln(10)
		
	def pdf_salida(self,ruta):
		
		pdf.output(ruta,'F')
		
	def pdf_detalles(self,autor,nombre_i):
		
		# Establecer margenes, numeros de pagina, tiulo y autor del documento
		pdf.set_margins(left=20,top=30,right=20)
		pdf.alias_nb_pages()
		pdf.set_title(nombre_i)
		pdf.set_author(autor)
		
	def vacio(self):
		
		# Si no existen vulnerabilidades
		self.set_font('Arial','',12)
		self.cell(10,6,"No existen vulnerabilidades en este archivo")
	
	def imprimir_pdf(self,autor,scan_final,nombre_i):
		
		# Condiciones para la impresion del documento
		
		global pdf
		pdf = PDF()
		cambio_ruta = None
		ruta = '/tmp/informe.pdf'
		pdf.pdf_detalles(autor,nombre_i)
		pdf.titulo_pdf(nombre_i)
		if scan_final[0] == [] and scan_final[1] == [] and scan_final[2] == [] and scan_final[3] == [] and scan_final[4] == []:
			pdf.vacio()
		num = 1
		if scan_final[0] != []:
			pdf.titulo_vuln(num,'SQL Injection')
			num = num+1
		for i in range(len(scan_final[0])):
			if scan_final[0] != [] and scan_final[0][i][1] != []:
				pdf.imprimir_vuln(scan_final[0][i][0],scan_final[0][i][1],scan_final[0][i][2],cambio_ruta)
				cambio_ruta = scan_final[0][i][0]
		if scan_final[1] != []:
			pdf.titulo_vuln(num,'XSS')
			num = num+1
		for i in range(len(scan_final[1])):
			if scan_final[1] != [] and scan_final[1][i][1] != []:
				pdf.imprimir_vuln(scan_final[1][i][0],scan_final[1][i][1],scan_final[1][i][2],False)
		cambio_ruta = None		
		if scan_final[2] != []:
			pdf.titulo_vuln(num,'Remote File Inclusion')
			num = num+1
		for i in range(len(scan_final[2])):
			if scan_final[2] != [] and scan_final[2][i][1] != []:
				pdf.imprimir_vuln(scan_final[2][i][0],scan_final[2][i][1],scan_final[2][i][2],cambio_ruta)	
				cambio_ruta = scan_final[2][i][0]
		cambio_ruta = None
		if scan_final[3] != []:
			pdf.titulo_vuln(num,'Local File Inclusion')
			num = num+1
		for i in range(len(scan_final[3])):
			if scan_final[3] != [] and scan_final[3][i][1] != []:
				pdf.imprimir_vuln(scan_final[3][i][0],scan_final[3][i][1],scan_final[3][i][2],cambio_ruta)
				cambio_ruta = scan_final[3][i][0]
		cambio_ruta = None
		if scan_final[4] != []:
			pdf.titulo_vuln(num,'Remote Command Execution')
			num = num+1
		for i in range(len(scan_final[4])):
			if scan_final[4] != [] and scan_final[4][i][1] != []:
				pdf.imprimir_vuln(scan_final[4][i][0],scan_final[4][i][1],scan_final[4][i][2],cambio_ruta)				
				cambio_ruta = scan_final[4][i][0]
		pdf.pdf_salida(ruta)
		return ruta
		
		

