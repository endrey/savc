import datos.conexion

class Perfil:
	
	def __init__(self):

		global conex
		conex=datos.conexion.Conexion("","")	
				
	def colocar_nueva_id(self,cedula,rol):
		
		# Colocar id de perfil a un nuevo usuario.
		
		lista=conex.consulta("select max(numero_perfil) from perfil","")
		if lista[0][0] != None:
			n_perfil = lista[0][0]+1
		else:
			n_perfil = 1
			
		conex.ejecutar("insert into usuario_perfil values (%s,%s)",\
		(cedula,n_perfil))
		
		if rol == "Super Administrador":
			conex.ejecutar("insert into perfil\
			 (numero_perfil,eliminar,analizar,registrar,modificar) \
			 values (%s,'1','1','1','1')",\
			 (str(n_perfil),))
		elif rol == "Administrador":
			conex.ejecutar("insert into perfil\
			 (numero_perfil,eliminar,analizar,registrar,modificar)\
			  values (%s,0,1,1,1)",\
			  (str(n_perfil),))
		else:
			conex.ejecutar("insert into perfil\
			 (numero_perfil,eliminar,analizar,registrar,modificar)\
			  values (%s,0,0,0,1)",\
			  (str(n_perfil),))
		
		conex.commit()
					
	def permisos_usuario(self,cedula,igual,igual1,igual2,igual3):
		
		# Editar el perfil de un usuario 
		
		conex.ejecutar("UPDATE perfil AS p SET registrar=%s,modificar=%s,eliminar=%s,analizar=%s\
		FROM usuario_perfil AS up, usuario AS u where up.usuario_cedula=u.cedula AND up.perfil_numero_perfil=p.numero_perfil\
		AND cedula=%s",(int(igual),int(igual1),int(igual2),int(igual3),cedula,))
		
		conex.commit()
		
	def obtener_perfil(self,cedula):
		
		perfil = conex.consulta("SELECT eliminar,registrar,modificar,analizar \
								FROM perfil, usuario_perfil, usuario \
								WHERE numero_perfil = perfil_numero_perfil \
								AND cedula = usuario_cedula \
								AND usuario_cedula = %s",(cedula,))
		return perfil[0]
	
		
		
		
